<?php

/**
 * Plugin Name: Vithas Form
 * Plugin URI: https://leangasoftware.es/
 * Description: Plugin especializado para la conexión PRM de Vithas
 * Version: 2.1
 * Author: Leifer Mendez
 * Author URI: https://leangasoftware.es/
 * License: A "Slug" license name e.g. GPL12
 */

require_once(__DIR__ . '/class/FormVithas.php');
require_once(__DIR__ . '/class/FiltersVithas.php');

class MainClassVithas extends FormVithas
{

    public static $DEBUG_MODE = true;
    public static $AUTO_GRID = true;
    public $filters;

    public function __construct()
    {
        global $wpdb, $template, $ts_mail_errors, $phpmailer, $product, $post, $woocommerce;

        $this->filters = new FiltersVithas();

        parent::__construct(
            $wpdb,
            $template,
            $ts_mail_errors,
            $phpmailer,
            $product,
            $post
        );


        self::$AUTO_GRID = (self::SettingData('auto_grid') && self::SettingData('auto_grid')->value === 'true')
            ? TRUE : FALSE;

        $file = basename(__FILE__);
        $folder = basename(dirname(__FILE__));

        $hook = "in_plugin_update_message-{$folder}/{$file}";

        $system = parent::Requirements()['system'];
        if ($system) {
            $system = json_encode($system);
            exit(sprintf('PHP Extension <b>' . $system . '</b> not found'));
        }

        if (is_admin()) {
            register_activation_hook(__FILE__, array($this, 'registerActivation'));
            register_deactivation_hook(__FILE__, array($this, 'dropSettingTable'));
        }

        if (self::$DEBUG_MODE) {
            add_action('wp_ajax_call_test', array($this, 'CallTest'));
            add_action('wp_ajax_nopriv_call_test', array($this, 'CallTest'));
        }

        //add_action('init', array($this, 'InitPlugin'));


        /** -------------------------------------------------------------- */


        add_action('admin_notices', array($this, 'Requirements'));

        add_action('admin_notices', array($this, 'CheckPages'));

        add_action('wp_enqueue_scripts', array($this, 'assets'));

        add_action('admin_enqueue_scripts', array($this, 'admin_assets'));

        add_action('admin_menu', array($this, 'AdminPage'));

        add_action('woocommerce_variation_options', array($this, 'NoticeSku'), 10, 3);

        add_action('wp_head', array($this, 'HeadHtml'));

        add_action('wp_head', array($this, 'LoadModalHtml'));

        add_filter('document_title_parts', array($this, 'SetTitlePage'));
        /**
         * AJAX
         */
        add_action('wp_ajax_get_gdrp', array($this, 'GetGDRP'));

        add_action('wp_ajax_nopriv_get_gdrp', array($this, 'GetGDRP'));

        add_action('wp_ajax_get_centros', array($this, 'GetAgencies'));

        add_action('wp_ajax_nopriv_get_centros', array($this, 'GetAgencies'));

        add_action('wp_ajax_send_mail', array($this, 'MailContact'));

        add_action('wp_ajax_nopriv_send_mail', array($this, 'MailContact'));

        add_action('wp_ajax_get_redsys_button', array($this, 'PayButtonAttr'));

        add_action('wp_ajax_nopriv_get_redsys_button', array($this, 'PayButtonAttr'));

        add_action('wp_ajax_get_init_form', array($this, 'GetInitForm'));

        add_action('wp_ajax_nopriv_get_init_form', array($this, 'GetInitForm'));

        add_action('wp_ajax_get_filters_data', array($this, 'GetFilters'));

        add_action('wp_ajax_nopriv_get_filters_data', array($this, 'GetFilters'));

        add_action('wp_ajax_get_by_key', array($this, 'GetByKey'));

        add_action('wp_ajax_nopriv_get_by_key', array($this, 'GetByKey'));

        add_action('wp_ajax_admin_save_centro', array($this, 'SaveCenter'));

        add_action('wp_ajax_admin_save_setting', array($this, 'SaveSettings'));

        add_action('rest_api_init', array($this, 'RestApiInit'));
        /**
         * [vithas-carousel]
         */
        add_shortcode('vithas-carousel', array($this, 'GetCarousel'));

        //add_shortcode('vithas-pay', array($this, 'PayButton'));
        /**
         * [vithas-form agency="2"]
         */
        add_shortcode('vithas-form', array($this, 'FormContact'));

        add_shortcode('vithas-filters', array($this, 'VithasFilters'));


        /*
         *  `[vithas-product product="2"]`
         */
        add_shortcode('vithas-product', array($this, 'GridProductVariations'));

        add_shortcode('vartable', array($this, 'AutoGrid'));

        add_filter('404_template', array($this, 'ThankYouPage'));

        if (self::$AUTO_GRID) {
            add_action('woocommerce_single_product_summary', array($this, 'AutoGrid'), 10);
        }

        add_action('woocommerce_single_product_summary', array($this, 'HidenID'), 6);
    }

    public function InitPlugin()
    {

        if (is_admin()) {

            $version = get_option('vithas-version', '0.0.0');
            if ($version == '0.0.0') {
                // store new version
                update_option('vithas-version', '3.0');
                // plugin upgraded ?
            } else if (version_compare('3.0', $version, '>')) {
                // run upgrade hook
                if (true) {
                    update_option('vithas-version', '3.0');
                }


            }
            // add plugin upgrade notification
            add_action('in_plugin_update_message-vithas-form/index.php', array($this, 'InlineNotice'), 10, 2);

            // plugin upgraded ?

        }
    }

    public function HidenID()
    {
        global $product;
        $product_id = $product->get_id();
        echo "<input type='hidden' id='hiden_id_product_vh' value='" . $product_id . "'>";
    }

    public function AutoGrid()
    {
        global $product;
        $id = $product->id;
        echo do_shortcode('[vithas-product product="' . $id . '"]');
    }

    /**
     * Plugin Activacion
     */

    public function registerActivation()
    {
        return parent::InstallDB();
    }

    public function dropSettingTable()
    {
        return parent::DropTableSetting();
    }

    public function InlineNotice($data, $response)
    {
        var_dump($data);
        var_dump($response);
    }

    /**
     * Funciones Generales
     */

    public function AdminPage()
    {
        add_menu_page(
            'Vithas Forms',
            'Vithas Forms',
            'manage_options',
            'vithas-forms',
            function () {
                parent::AdminPage();
            },
            'dashicons-media-text'
        );

        add_submenu_page(
            'vithas-forms',
            'Centros',
            'Centros',
            'manage_options',
            'vithas-list-centros',
            function () {
                parent::AdminPageList();
            });

        add_submenu_page(
            'vithas-forms',
            'Agregar',
            'Agregar',
            'manage_options',
            'vithas-add-centros',
            function () {
                parent::AdminPageInside();
            });

        add_submenu_page(
            'vithas-forms',
            'Documentacion',
            'Documentacion',
            'manage_options',
            'vithas-documentation',
            function () {
                parent::AdminDocumentation();
            });

        add_submenu_page(
            'vithas-forms',
            'Ajustes',
            'Ajustes',
            'manage_options',
            'vithas-settings',
            function () {
                parent::AdminSettings();
            });
    }

    public function SaveCenter()
    {
        return parent::SaveCenter();
    }

    public function SaveSettings()
    {
        return parent::SaveSettings();
    }

    public function HeadHtml()
    {
        return parent::HeadHtml();
    }

    public function LoadModalHtml()
    {
        parent::LoadModalHtml();
    }

    public function FormContact($atts = array())
    {
        $id = (isset($atts['agency'])) ? $atts['agency'] : null;
        parent::FormContact($id);
    }

    public function GetCarousel()
    {
        return parent::GetAgenciesCarousel();

    }

    public function GetInitForm($id=null)
    {
        $id = $_GET['id'];
        $res = parent::GetInitForm($id);
        $res = json_encode($res);
        echo $res;
        wp_die();
    }

    public function GetGDRP()
    {
        $res = parent::GetGDRP();
        echo json_encode($res);
        wp_die();
    }

    public function GridProductVariations($atts = null)
    {
        $id = (isset($atts['product'])) ? $atts['product'] : null;
        return parent::GridProductVariations($id);
    }

    public function GetAgencies()
    {
        return parent::GetAgencies();
    }

    public function admin_assets()
    {

        wp_register_script('script_js_vithas_admin_js', plugin_dir_url(__FILE__) . 'assets/js/script-admin.js', array(), '1.0.0', false);
        wp_enqueue_script('script_js_vithas_admin_js');

        $translation_array = array('siteUrl' => get_site_url());
        wp_localize_script('script_js_vithas_admin_js', 'backend_data', $translation_array);
    }

    public function assets()
    {

        /* JAVASCRIPT */

        if (parent::SettingData('jquery') && parent::SettingData('jquery')->value === 'true') {
            wp_register_script('script_js_vithas_jq', '//code.jquery.com/jquery-1.12.4.min.js', array(), '1.0.0', true);
            wp_enqueue_script('script_js_vithas_jq');
        }

        wp_register_script('script_js_vithas_form_modal', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js', array(), '1.0.0', true);
        wp_enqueue_script('script_js_vithas_form_modal');

        wp_register_script('script_js_vithas_form_owl', plugin_dir_url(__FILE__) . 'assets/js/pkg/owl.carousel.min.js', array(), '1.0.0', true);
        wp_enqueue_script('script_js_vithas_form_owl');

        wp_register_script('script_js_vithas_main', plugin_dir_url(__FILE__) . 'assets/js/main.script.js', array(), '1.0.0', false);
        wp_enqueue_script('script_js_vithas_main');

        wp_register_script('script_js_vithas_form', plugin_dir_url(__FILE__) . 'assets/js/script.js', array(), '1.0.0', true);
        wp_enqueue_script('script_js_vithas_form');

        $translation_array = array('siteUrl' => get_site_url());
        wp_localize_script('script_js_vithas_main', 'backend_data', $translation_array);

        wp_register_script('script_js_vithas_footer', plugin_dir_url(__FILE__) . 'assets/js/footer.script.js', array(), '1.0.0', true);
        wp_enqueue_script('script_js_vithas_footer');

        /*STYLE CSS*/
        wp_register_style('style_css_vithas_form_modal', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css');
        wp_enqueue_style('style_css_vithas_form_modal');

        wp_register_style('style_css_vithas_form_owl', plugin_dir_url(__FILE__) . 'assets/css/owl.carousel.min.css');
        wp_enqueue_style('style_css_vithas_form_owl');

        wp_register_style('style_css_vithas_form', plugin_dir_url(__FILE__) . 'assets/css/style.css');
        wp_enqueue_style('style_css_vithas_form');

        wp_register_style('style_css_vithas_extra', plugin_dir_url(__FILE__) . 'assets/css/extra.css');
        wp_enqueue_style('style_css_vithas_extra');

        parent::Log('Assets Loaded');

    }

    public function RestApiInit()
    {
        parent::Log('RestAPI WH Call');

        register_rest_route('vithas-form', '/payment', array(
            'methods' => 'GET',
            'callback' => array($this, 'WebHook'),
        ));


        register_rest_route('vithas-form', '/payment', array(
            'methods' => 'POST',
            'callback' => array($this, 'WebHook'),
        ));


    }

    public function WebHook($request = null)
    {
        $data = $request->get_params();
        parent::Log('WebHook Call');
        return parent::WebHook($data);
    }

    /**
     * SOAP PRM
     */

    public function MailContact()
    {
        $data = array(
            'name' => $_POST['name'],
            'last_name' => $_POST['last_name_1'],
            'phone' => $_POST['phone'],
            'email' => $_POST['email'],
            'centro' => $_POST['centro'], //20
            'type' => $_POST['type']
        );

        $result = parent::GetAgency($data['centro']);
        $list_mails = '';
        if ($result) {
            $centro_email = $result->mail;
            $centro_email_cc = $result->mail_cc;
            $mails = explode(',', $centro_email);
            $mails_cc = explode(',', $centro_email_cc);
            $list_mails = array_merge($mails, $mails_cc);
            $list_mails = implode(',', $list_mails);
        };

        /**
         * Aqui se debe especificar el cuerpo del mail en array, cada elemento es una linea en el mail.
         * Para: (wil)
         */


        /**
         * Cuerpo mail para contacto general
         */
        $body_mail = [
            parent::$MAIL_CONTACT_SUBJECT,
            '',
            "Hola {$data['name']} {$data['last_name']}",
            "Hemos recibido correctamente tu solicitud de reserva de cita. En menos de 24 horas un asesor persona 
                de salud se pondrá en contacto contigo para concretar los detalles de la misma",
            '',
            'El equipo de Vithas'
        ];

        /**
         * Cuerpo mail para reserva
         */

        if ($data && $data['type'] === 'appointment') {
            $body_mail = [//Vithas
                parent::$MAIL_CONTACT_APPOINTMENT,
                '',
                "Hola {$data['name']} {$data['last_name']}",
                "ha realizado una solicitud de reserva de cita con los siguientes datos",
                '',
                "Nombre y Apellidos: {$data['name']} {$data['last_name']}",
                "Email: {$data['email']}",
                "Teléfono: {$data['phone']}",
                "Producto:",
                "Fecha de solicitud: ",
                "Centro: {$result->tsne}",
                '',
                'El equipo de Vithas'
            ];
        }

        $res = parent::MailContact(
            $body_mail,
            explode(',',$list_mails.$data['email']),
            ($data && $data['type'] === 'appointment') ? parent::$MAIL_CONTACT_APPOINTMENT : parent::$MAIL_CONTACT_APPOINTMENT,
            null,
            $data,
            true
        );


        echo json_encode($res);
        wp_die();
    }

    /**
     * Redsys
     * @param $atts
     * @return string
     */

    public function PayButton($atts = array())
    {

        if (count($_POST)) {
            $data = array(
                'id' => $_POST['id'],
                'amount' => $_POST['amount'],
                'redirect' => $_POST['redirect']
            );
        } else {
            $data = array(
                'id' => $atts['id'],
                'amount' => $atts['amount'],
                'redirect' => $atts['redirect']
            );
        }

        $button = parent::GetRedsysButton($data['id'], $data['amount'], $data, $data['redirect'], FALSE);
        if ($_POST['action']) {
            echo $button;
            wp_die();
        } else {
            return $button;
        }
    }

    public function PayButtonAttr()
    {


        $data = array(
            'id' => $_POST['id'],
            'amount' => $_POST['amount'],
            'redirect' => $_POST['redirect'],
            'customer' => $_POST['customer'],
            'center' => $_POST['center'],
            'product_id' => $_POST['product_id']
        );

        $attr = self::DataPrice($data['id']);
        $button = parent::GetRedsysButton($attr['index'], $attr['price'], $data['customer'], $data['redirect'], FALSE);
        echo $button;
        wp_die();
    }

    public function SetTitlePage($title = NULL)
    {
        if (is_404()) {
            $title_parts['title'] = 'Gracias!';
        }

        return $title_parts;
    }

    public function ThankYouPage()
    {
        return parent::ThankYouPage();
    }


    /**
     * Funciones Filtros
     */

    public function VithasFilters($atts = array())
    {
        /*
         * ['find' => ['Centros']]
         */

        $html = $this->filters->GetFilters($atts);

        return $html;
    }

    /**
     * Funciones para pruebas internas
     */

    public function CheckPages()
    {
        $res = parent::CheckPages();
        if ($res) {
            echo $res;
        } else {
            echo '';
        }
    }

    public function Requirements()
    {
        $res = parent::Requirements();
        if ($res['system']) {
            echo $res['html'];
        } else {
            echo '';
        }

    }

    public function NoticeSku()
    {
        parent::NoticeSku();
    }

    public function TestSystem()
    {
        $system = array(
            'curl' => function_exists('curl_version'),
            'soap' => extension_loaded('soap'),
        );

        $log = json_encode($system);
        parent::Log('*** TEST ***');

        return $system;
    }

    public function GetFilters()
    {
        $result = $this->filters->DataFilter($_GET['filters']);
        $result = $this->filters->LoadHtmlFilter($result);

        echo $result;
        wp_die();
    }

    public function GetByKey()
    {
        try {
            $val = $_GET['value'];
            switch ($_GET['key']) {
                case 'district':
                    $data = $this->filters->GetAllByTerm('taxonomy', 'provincia');
                    break;
                case 'centers':
                    $data = $this->filters->CentersBy('district', $val);
                    break;
                case 'target':
                    $data = $this->filters->CentersBy('tsne', $val);
                    if ($data && isset($data[0])) {
                        $data = explode(',', $data[0]->specialties);
                    } else {
                        $data = [];
                    }

                    break;
                case 'servicio':
                    $data = $this->filters->CentersBy('type_service', $val);
                    if ($data && isset($data[0])) {
                        $data = explode(',', $data[0]->type_service);
                    } else {
                        $data = [];
                    }
                    break;
            }

            echo json_encode($data);
            wp_die();

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function CallTest()
    {

        /*
         * Se utlizar para realizar test desde una peticion GET
         *
         *
         */

        $res = self::GetHospitals();
        var_dump($res);




    }


}


new MainClassVithas();