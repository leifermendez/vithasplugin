## Instrucciones

El plugin aun se encuentra en desarrollo. Pero ya podemos hacer uso de las principales funciones.

__NOTA:__ Rogame encarecidamente ver el [Wiki](https://gitlab.com/leifermendez/vithasplugin/wikis/pages) para su uso recomendado.

## Requerimientos

 - Extension Curl de PHP
 - Extension SOAP de PHP
 - PHP 7.1 o superior
 - Woocommerce
 
## Instalación
    
 Al momento de activar el plugin, este creara 3 tablas en la BD.
 
 - __vithas_agencies__ 
  `Encargada de almacenar todos los hospitales disponibles con sus correspondientes
  datos, (nombre, email, redsys_key, redsys_fuc, entre otros)`
 - __vithas_user_payments__ 
 `Encargada de guardar todos los movimientos e intentos de movimientos relacionados a Redsys`
 - __vithas_settings__ 
 `Encargada de almacener ajustes necesarios para el sistema`
 
## Uso

 A continuacion encontraras disparadores y shortocodes del plugin

#### Modales

 - __Modal Contacto General__:
    
    > Para abrir un modal de contacto general
    solo debes de agregar la clase "v2-vh-modal" al elementos html.
    Ejemplo: __`<button class="v2-vh-modal">ABRIR MODAL</button>`__
 
 - __Modal con boton de Pago__:
    
    > Para abrir un modal con boton de pago
    aparte de colocar la clase "v2-vh-modal" de agregar el atributo __data-id-attr__ con el valor
    __del atributo del woocommerce__.
    Ejemplo: __`<button class="v2-vh-modal" data-id-attr="13">PAGAR (ATTR:13)</button>`__
 
#### ShortCode
 - __Carousel de Centros__
 
    > __`[vithas-carousel]`__ Pintar carousel de centros
 - __Formulario de contacto__
 
    > __`[vithas-form agency="2"]`__ Insertar un formulario de contacto, 
    puedes pasar el atributo "agency" con el id del centro para especificar a quien va
    dirigido. Si no pasas el atributo se cargara una lista con los centros disponibles.
 - __Variaciones en producto__ 
    > `[vithas-product product="2"]` Inserta un grid con los botons reservar o pagar, solo debes pasar el id de producto.
#### SKU
 - __Nomenclatura__
    > El SKU de las variaciones de los productos son de total prioridad,
     ya que con ellos realizaremos la relacion con el ID del centro.
    Ejemplo: __`A_B_C`__
    - A:
        > En la posicion A, la utlizaremos para colocar un numero, letra o cadena aleatoria la 
            cual nos sirve para evitar SKU repetidos
    - B:
        > Colocaremos el ID del centro registrado en nuestra tabla `vithas_agencies`
    - C:
        > La usaremos determinar si un producto se necesitar pagar o solo reservar
        `1 = Reservar + Pagar` y `0 = Solo reservar`
    
    > Ejemplo:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
      - A123_10_1: Centro con el ID (10), reserva + pago.
      - A657_10_0: Centro con el ID (10), solo reserva.
### Preview

![MODAL_GIF](https://gitlab.com/leifermendez/vithasplugin/raw/master/readme/modal_open_vh.gif)

![MODAL_GIF](https://gitlab.com/leifermendez/vithasplugin/raw/master/readme/modal_pay.gif)


desarrollado por [Leifer](https://leifermendez.github.io).