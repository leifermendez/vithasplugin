/**
 * Plugin: VithasForm
 * Author: https://github.com/leifermendez
 */

main_functions[mainID] = {
    openModal: function (v) {
        id_attr = jQuery(v).attr("data-id-attr")
        hide_pay = jQuery(v).attr("data-hide-pay")
        idCenter = jQuery(v).attr("data-id-center")
        changeText = jQuery(v).attr("data-id-text")

        jQuery(`${mainID} .vh-loading`).hide();
        jQuery(`${mainID} .gprd`).hide();
        main_functions[mainID].validForm();
        main_functions[mainID].getCenters();
        if (id_attr) jQuery(`${mainID} form #centro`).hide();
        if (id_attr) jQuery(`${mainID} ._clase_centro`).hide();
        if (id_attr) jQuery(`${mainID} .vh-pay-button-redsys`).hide();
        if (id_attr) main_functions[mainID].getInitForm();
        //this.blur();


        if (changeText) {
            console.log('--', changeText)
            switch (changeText) {
                case 'form_type_1':
                    jQuery(`${mainID} #form_type_1`).removeClass('d-none');
                    jQuery(`${mainID} #form_type_2`).addClass('d-none');
                    jQuery(`${mainID} #form_type_3`).addClass('d-none');
                    jQuery(`${mainID} #form_type_4`).addClass('d-none');
                    jQuery(`${mainID} #enviar_btn`).removeClass('d-none');
                    break;
                case 'form_type_2':
                    jQuery(`${mainID} #form_type_1`).addClass('d-none');
                    jQuery(`${mainID} #form_type_2`).removeClass('d-none');
                    jQuery(`${mainID} #form_type_3`).addClass('d-none');
                    jQuery(`${mainID} #form_type_4`).addClass('d-none');
                    jQuery(`${mainID} #enviar_btn`).addClass('d-none');
                    break;
                case 'form_type_3':
                    jQuery(`${mainID} #form_type_1`).addClass('d-none');
                    jQuery(`${mainID} #form_type_2`).addClass('d-none');
                    jQuery(`${mainID} #form_type_3`).removeClass('d-none');
                    jQuery(`${mainID} #form_type_4`).addClass('d-none');
                    jQuery(`${mainID} #enviar_btn`).removeClass('d-none');
                    break;
                case 'form_type_4':
                    jQuery(`${mainID} #form_type_1`).addClass('d-none');
                    jQuery(`${mainID} #form_type_2`).addClass('d-none');
                    jQuery(`${mainID} #form_type_3`).addClass('d-none');
                    jQuery(`${mainID} #form_type_4`).removeClass('d-none');
                    jQuery(`${mainID} #enviar_btn`).removeClass('d-none');
                    break;
                default:
                    jQuery(`${mainID} #form_type_1`).removeClass('d-none');
                    jQuery(`${mainID} #form_type_2`).addClass('d-none');
                    jQuery(`${mainID} #form_type_3`).addClass('d-none');
                    jQuery(`${mainID} #form_type_4`).addClass('d-none');
                    jQuery(`${mainID} #enviar_btn`).removeClass('d-none');
            }
        } else {
            jQuery(`${mainID} #form_type_1`).removeClass('d-none');
            jQuery(`${mainID} #form_type_2`).addClass('d-none');
            jQuery(`${mainID} #form_type_3`).addClass('d-none');
            jQuery(`${mainID} #form_type_4`).addClass('d-none');
        }


        const continue_pay = jQuery(`${mainID}  #continue_pay`)
        continue_pay.hide();

        if (mainID === '#v2-vithas-modal') {
            jQuery(mainID).appendTo('body').modal({
                fadeDuration: 130
            });
        }
        if (id_attr) {
            continue_pay.show();
        }
    },
    getCenters: function () {
        jQuery(`${mainID} .vh-loading`).show();
        jQuery.get(
            endPoint,
            {
                'action': 'get_centros',
                'redirect_to': window.location.href
            },
            function (response) {
                jQuery(`${mainID} .vh-loading`).hide();
                response = JSON.parse(response);
                if (response && response['data']) {
                    jQuery(`${mainID} #centro`).empty();

                    const o_ = new Option('Centro', 'init');
                    jQuery(o_).html('Centro');
                    jQuery(`${mainID} #centro`).append(o_);
                    response['data'].map(p => {
                        const o = new Option(p.id, p.tsne);
                        jQuery(o).html(p.tsne);
                        jQuery(`${mainID} #centro`).append(o);
                    });

                }
            }
        );
    },
    sendContact: function (id = null) {
        jQuery(`${mainID} .vh-loading`).show();
        const data = {
            email: jQuery(`${mainID} form #email`).val(),
            name: jQuery(`${mainID} form #name`).val(),
            last_name_1: jQuery(`${mainID} form #last_name_1`).val(),
            last_name_2: jQuery(`${mainID} form #last_name_2`).val(),
            phone: jQuery(`${mainID} form #phone`).val(),
            redirect_to: window.location.href,
   			centro: (idCenter) ? idCenter : jQuery(`${mainID} form #centro`).val(),
            type: (id_attr) ? 'appointment':null
        }
  console.log('---',data)
        jQuery.post(
            endPoint,
            {...{action: 'send_mail'}, ...data},
            function (response) {
                // jQuery('.form-general-vithas #redsys_action_space').html(response);
                jQuery(`${mainID} .vh-loading`).hide();
                jQuery(`${mainID} form`).trigger("reset");
                jQuery(`${mainID} .close-modal`).click();
                let url_clean = `${window.location.href}/${thankYouPageContact}`
                url_clean = url_clean.replace('#', '');
                window.location.href = url_clean;
            }
        );
    },
    getButtonRedsys: function (id = null, amount = 0, redirect = null) {
        const res = main_functions[mainID].validForm('disabled');
        jQuery(`${mainID} .vh-loading`).show();
        redirect = (!redirect) ? `${window.location.href}/${thankYouPagePayment}` : redirect;
        const data = {
            id, amount, redirect,
            customer: {
                email: jQuery(`${mainID} form #email`).val(),
                name: jQuery(`${mainID} form #name`).val(),
                last_name_1: jQuery(`${mainID} form #last_name_1`).val(),
                last_name_2: jQuery(`${mainID} form #last_name_2`).val(),
                phone: jQuery(`${mainID} form #phone`).val(),
                redirect_to: window.location.href,
                product_id: jQuery(`body #hiden_id_product_vh`).val()
            }
        }

        console.log('data', res)
        jQuery.post(
            endPoint,
            {...{action: 'get_redsys_button'}, ...data},
            function (response) {
                jQuery(`${mainID}  #redsys_action_space`).html(response);

                jQuery(`${mainID} .vh-loading`).hide();
                jQuery(`${mainID} .vh-pay-button-redsys`).show();
                main_functions[mainID].validForm();
                //jQuery('.close-modal').click();
            }
        );
    },
    validForm: function (type = null) {
        const form = document.querySelector(modalClassForm);
        const isValidForm = form.checkValidity();
        const iframeRedsys = document.querySelector(`${mainID} #iframe_redsys`);
        if (iframeRedsys) {
            let clicked = false;
            let loading = false;
            setInterval(function () {
                if (!clicked) {
                    /*iframeRedsys.contentWindow.document.getElementsByClassName('btn')[0].click();
                    clicked = true;
                    console.log('click', clicked)
                    jQuery(`${mainID} #redsys_action_space`).html(`Redireccionando...`);*/
                }

                if (!loading) {
                    iframeRedsys.contentWindow.document.body.onclick = function () {
                        jQuery(`${mainID} #redsys_action_space`).hide();
                        //jQuery(`${mainID} .vh-loading`).show();
                        loading = !loading

                    };
                }


            }, 500);
        }

        jQuery(`${modalClassForm} :input`).each(function () {
            const input = jQuery(this); // This is the jquery object of the input, do what you will

            if (!type) {
                //jQuery(modalClassForm).trigger("reset");
                input.removeClass('is-invalid');
                input.prop("disabled", false);
            } else if (type === 'valid') {
                if (isValidForm) {
                    input.removeClass('is-invalid');
                    //jQuery(modalClassForm).trigger("reset");
                } else {
                    input.addClass('is-invalid');
                }
            } else if (type === 'disabled') {
                input.prop("disabled", true);
                //jQuery(modalClassForm).trigger("reset");
            }
        });

        return isValidForm;
    },
    getGDRP: function () {
        jQuery(`${mainID} .vh-loading`).show();
        jQuery.get(
            endPoint,
            {
                'action': 'get_gdrp'
            },
            function (response) {
                jQuery(`${mainID} .vh-loading`).hide();
                jQuery(`${mainID} .gprd`).fadeIn();
                response = JSON.parse(response);
                jQuery(`${mainID} #gprd-content`).html(response['value']);
            });
    },
    getInitForm: function () {
        jQuery(`${mainID} .vh-loading`).show();
        jQuery.get(
            endPoint,
            {
                'action': 'get_init_form',
                'id': id_attr
            },
            function (response) {
                jQuery(`${mainID} .vh-loading`).hide();
                response = JSON.parse(response);
                if (id_attr) jQuery(`${mainID} form #centro`).val(response['center']);
                if (response && response['pay'] && !hide_pay) {
                    jQuery(`${mainID}  #continue_pay`).show();
                } else {
                    jQuery(`${mainID}  #continue_pay`).hide();
                }
            });
    }
};

getByKey = (key = null, value = null) => new Promise((resolve, reject) => {
    jQuery.get(
        endPoint,
        {
            'action': 'get_by_key',
            key, value
        },
        function (response) {
            resolve(response)
        });
});

function getDataFilter(opt = []) {
    jQuery(`${filterResponseID} .response-parent-vh`).show();
    let ful = '';
    opt.map(a => {
        ful += `${a['key']}=${a['value']}|`;
    });

    jQuery.get(
        endPoint,
        {
            'action': 'get_filters_data',
            'filters': ful
        },
        function (response) {
            jQuery(`${filterResponseID} .response-parent-vh`).hide();
            const response_div = jQuery(`#filter_response`);

            response_div.html(response)
        });
}

function triggerFilter(slug = null) {
    if (slug && JSON.parse(slug)) {
        slug = JSON.parse(slug);
        pre_data_filter = Object.keys(slug);
    } else {
        pre_data_filter = false;
    }

    const select_district = jQuery(`${filterID} #select_district`);
    if (select_district.val() === 'init') {
        getByKey('district').then((r) => {
            const res = JSON.parse(r);
            select_district.find('option').remove();
            const o_ = new Option('Provincia', 'init');
            jQuery(o_).html('Provincia');
            select_district.append(o_);
            res.map(p => {
                const o = new Option(p.description, p.description);
                jQuery(o).html(p.description);
                select_district.append(o);
            });

        })
    }

    if (slug) {
        Search(slug);
    }


}

function Search(pre_search = null) {

    const select_district = jQuery(`${filterID} #select_district`);
    const select_center = jQuery(`${filterID} #select_center`);
    const select_target = jQuery(`${filterID} #select_target`);
    const select_type = jQuery(`${filterID} #select_type`);

    if (!pre_data_filter && !pre_search) {
        f = [];
    } else if (pre_data_filter && !pre_search) {
        const p = f.findIndex(a => a['key'] === "especialidad")
        const tmp = f[p];
        console.log(tmp)
        f = [tmp];
    }

    console.log(pre_search)
    console.log(pre_data_filter)

    if (pre_search && Object.values(pre_search).length) {

        if (pre_search['select_target']) {
            f.push({
                key: 'especialidad',
                value: pre_search['select_target']
            })
        }
    }

    if (select_district.val() && select_district.val() !== 'init') {
        f.push({
            key: 'provincia',
            value: select_district.val()
        })
    }
    if (select_center.val() && select_center.val() !== 'init') {
        f.push({
            key: 'centros',
            value: select_center.val()
        })
    }
    if (select_target.val() && select_target.val() !== 'init') {
        f.push({
            key: 'especialidad',
            value: select_target.val()
        })
    }
    if (select_type.val() && select_type.val() !== 'init') {
        f.push({
            key: 'servicio',
            value: select_type.val()
        })
    }

    console.log(f)

    getDataFilter(f);
}

jQuery(function ($) {

    const select_district = jQuery(`${filterID} #select_district`);
    const select_center = jQuery(`${filterID} #select_center`);
    const select_target = jQuery(`${filterID} #select_target`);
    const select_type = jQuery(`${filterID} #select_type`);

    select_district.change(function () {
        console.log('centors en ,', jQuery(this).val())
        getByKey('centers', jQuery(this).val()).then((r) => {
            const res = JSON.parse(r);
            select_center.find('option').remove();
            const o_ = new Option('Centro', 'init');
            jQuery(o_).html('Centro');
            select_center.append(o_);
            res.map(p => {
                const o = new Option(p.tsne, p.tsne);
                jQuery(o).html(p.tsne);
                select_center.append(o);
            });


        })
    });

    select_center.change(function () {
        console.log('escpecial en ,', jQuery(this).val())
        getByKey('target', jQuery(this).val()).then((r) => {
            const res = JSON.parse(r);
            select_target.find('option').remove();
            const o_ = new Option('Tipo de servicio', 'init');
            jQuery(o_).html('Tipo de servicio');
            select_target.append(o_);
            res.map(p => {
                const o = new Option(p, p);
                jQuery(o).html(p);
                select_target.append(o);
            });
        })
    });

    getByKey('servicio', null).then((r) => {

        const res = JSON.parse(r);
        console.log('r', res)
        select_type.find('option').remove();
        const o_ = new Option('Tipo de servicio', 'init');
        jQuery(o_).html('Tipo de servicio');
        select_type.append(o_);
        res.map(p => {
            console.log(p)
            const o = new Option(p, p);
            jQuery(o).html(p);
            select_type.append(o);
        });
    });
    /* select_target.change(function () {
         console.log('tipo en ,', jQuery(this).val())

     });*/

    jQuery(document).on('click', modalClass, function () {
        main_functions[mainID].openModal(this);
    });

    jQuery(document).on('click', `${filterID} #search_filter`, function () {
        Search();
    });

    jQuery(`${mainID} #datos_protect`).change(function () {
        if (this.checked) {
            main_functions[mainID].getGDRP();
        }
    });

    jQuery(document).on('submit', `${mainID} form`, function (event) {
        event.preventDefault();
        const id_centro = jQuery(`${mainID} #centro`).val();
        main_functions[mainID].sendContact(id_centro)
    });

    jQuery(`${mainID} #continue_pay button`).on('click', function (event) {
        event.preventDefault();
        const valid = main_functions[mainID].validForm('valid');
        if (valid) main_functions[mainID].getButtonRedsys(id_attr);
    });

    jQuery(`${mainID} .gprd button`).on('click', function (event) {
        event.preventDefault();
        jQuery(`${mainID} .gprd`).fadeOut();
    });


    if ((mainID !== '#v2-vithas-modal') && (jQuery(`${mainID} form #centro`).val()) !== undefined) {
        console.log('Loaded Form', mainID);
        console.log('Loaded Form Cneter', `${mainID} form #centro`);
        console.log('Existe field',);
        main_functions[mainID].getCenters();
    }

});

