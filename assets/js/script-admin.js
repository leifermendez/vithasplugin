jQuery(function ($) {
    let sku, select, tmp_product_services = [];

    siteUrl = backend_data.siteUrl;
    url_site_pro = siteUrl + '/wp-admin/admin-ajax.php';


    jQuery(`body #edittag #description`).prop('disabled', true);

    jQuery(document.body).on('change', '#sku_select_vh', function () {
        select = jQuery(this);
        const t = jQuery(this).parent();
        const t1 = jQuery(t).parent();
        const t2 = jQuery(t1).parent();
        const t3 = jQuery(t2).parent();
        t3.find('input[name^="variable_sku"]').each(function () {
            sku = jQuery(this);
            const tmp = sku.val().split('_');
            sku.val(`${tmp[0]}_${select.val()}_1`);
            //jQuery('#example_sku_vh').html('hola')
        });
        /*jQuery(t2).find('input[name^="pages_title"]').each(function() {
            console.log($(this).val())
        });*/

    });

    jQuery(document.body).on('change', '#_center_guid', function () {
        const select = jQuery(this);
        const type_services = jQuery(`#_center_type`);


        type_services.find('option').remove();
        const o_ = new Option('Seleccionar', null);
        jQuery(o_).html('Seleccionar');
        type_services.append(o_);

        get_data(select.val()).then(res => {

            if (res && res.ws_services) {
                const ws_services = JSON.parse(res.ws_services);
                tmp_product_services = JSON.parse(res.ws_specialties);

                ws_services.map(p => {
                    const o = new Option(p.vit_name, p.vit_name);
                    jQuery(o).html(p.vit_name);
                    type_services.append(o);
                });
            }
        })


    });


    jQuery(document.body).on('change', '#_center_type', function () {
        const my_option = jQuery(this).val();
        const product = jQuery(`#_center_product`);

        let tmp_parse_product = [];

        console.log(tmp_product_services)
        if (tmp_product_services && tmp_product_services['Servicio']) {
            console.log('--->', my_option)
            product.find('option').remove();
            const o_ = new Option('Seleccionar', null);
            jQuery(o_).html('Seleccionar');
            product.append(o_);
            Object.values(tmp_product_services['Servicio'][my_option]).map(p => {
                console.log(p)
                const o = new Option(p['vit_name'], p['Id']);
                 jQuery(o).html(p['vit_name']);
                 product.append(o);
            })
        }
        /*Object.values(tmp_product_services).map(p => {
             if (p) {
                 p.map(_p => {
                     console.log(_p)
                     console.log('my_option: ', my_option);
                     if (_p['Id'] === my_option) {
                         const o = new Option(_p['vit_name'], _p['Id']);
                         jQuery(o).html(_p['vit_name']);
                         product.append(o);
                     }
                 })
             }


         })*/

        // console.log(tmp_product_services)

        /* tmp_product_services.map(p => {

         });*/
    });

});

function _send() {
    jQuery.post(
        url_site_pro,
        {
            'action': 'admin_save_centro',
            'id': jQuery('#add-form #id').val(),
            'tsne': jQuery('#add-form #tsne').val(),
            'phone': jQuery('#add-form #phone').val(),
            'mail': jQuery('#add-form #mail').val(),
            'mail_cc': jQuery('#add-form #mail_cc').val(),
            'text': jQuery('#add-form #text').val(),
            'extra': jQuery('#add-form #extra').val(),
            'id_attr': jQuery('#add-form #id_attr').val(),
            'redsys_id': jQuery('#add-form #redsys_id').val(),
            'redsys_terminal': jQuery('#add-form #redsys_terminal').val(),
            'redsys_currency': jQuery('#add-form #redsys_currency').val(),
            'redsys_env': jQuery('#add-form #redsys_env').val(),
            'redsys_key': jQuery('#add-form #redsys_key').val(),
            'district': jQuery('#add-form #district').val(),
            'specialties': jQuery('#add-form #specialties').val(),
            'type_service': jQuery('#add-form #type_service').val(),
        },
        function (response) {
            response = JSON.parse(response);
            if (response.status === 'success') {
                window.location.href = `${window.location.origin}/wp-admin/admin.php?page=vithas-list-centros`
            }
        }
    );
}

function save_setting() {
    jQuery.post(
        url_site_pro,
        {
            'action': 'admin_save_setting',
            'fields':
                {
                    redsys_ws: jQuery('#setting_submit #redsys_ws').val(),
                    auto_grid: jQuery('#setting_submit #auto_grid').val(),
                    jquery: jQuery('#setting_submit #jquery').val(),
                    terms_conditions: tinymce.activeEditor.getContent({format: 'raw'})
                }

        },
        function (response) {
            response = JSON.parse(response);
            if (response.status === 'success') {
                window.location.reload();
            }
        }
    );
}


function get_data(id = null) {

    return new Promise(function (resolve, reject) {
        jQuery.get(
            url_site_pro,
            {
                'action': 'admin_get_catalogue',
                id

            },
            function (response) {
                response = JSON.parse(response);
                resolve(response);
            }
        );
    });
}