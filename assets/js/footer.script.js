initializeCarousel = function () {
    jQuery('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    })
};


window.onload = function () {
    if (window.jQuery) {
        initializeCarousel()
    } else {
        console.log('>>>>>>>>>>>>> JQUERY no installed?  <<<<<<<<<<<<<');
    }
};
