<?php

/*
 * Class: Redys
 */

require_once(__DIR__ . '/SettingField.php');
require_once(__DIR__ . '/../requires/redsysHMAC256_API_PHP_7.0.0/apiRedsys.php');

class RedsysVithas extends SettingField
{
    private static $endpoint = 'https://sis.redsys.es/sis/realizarPago';
    private static $endpointSandBox = 'https://sis-t.redsys.es:25443/sis/realizarPago';
    private static $wpdb;
    private static $hmac_version = 'HMAC_SHA256_V1';
    private $redsys;
    private $date;

    public function __construct($wpdb)
    {
        $this->redsys = new RedsysAPI;
        self::$wpdb = $wpdb;
        $this->date = new DateTime();
    }

    private function GetValueSetting($field = NULL)
    {
        try {
            $wpdb = self::$wpdb;
            $table = $wpdb->prefix . parent::$DB_SETTINGS;
            $sql = 'SELECT value FROM ' . $table . ' WHERE field = "' . $field . '"';
            $data = $wpdb->get_results($sql);
            return (count($data)) ? $data[0]->value : NULL;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function GetDataRedsys($fuc = NULL, $type = TRUE)
    {
        try {
            $wpdb = self::$wpdb;
            $table = $wpdb->prefix . parent::$DB_AGENCIES;
            $sql_1 = 'SELECT * FROM ' . $table . ' WHERE redsys_id = "' . $fuc . '"';
            $sql_2 = 'SELECT * FROM ' . $table . ' WHERE id = "' . $fuc . '"';
            $sql = ($type) ? $sql_1 : $sql_2;
            $data = $wpdb->get_results($sql);
            return (count($data)) ? $data[0] : [];

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function GetRedsysButton(
        $fuc = NULL,
        $amount = 0,
        $customer = array(),
        $redirectTo = null,
        $type = TRUE
    )
    {
        try {
            $wpdb = self::$wpdb;
            $id = time();
            $trans = "0";
            $url = $this->GetValueSetting('redsys_ws');
            $dataRedsys = self::GetDataRedsys($fuc, $type);
            $response = 'Redsys no encontrado (' . $fuc . ') <!-- Recuerda colocar el SKU X_IDCENTRO_ -->';

            if (count($dataRedsys)) {

                $endpoint = ($dataRedsys->redsys_env === 'test') ? self::$endpointSandBox : self::$endpoint;
                $urlOKKO = $redirectTo . 'compra_confirmada?id=' . base64_encode($id);

                $amount = floatval($amount * 100);
                $data_operation = explode("/", $redirectTo);
                /*unset($data_operation[0]);
                unset($data_operation[1]);
                unset($data_operation[2]);*/

                $data_operation = join("/", $data_operation);
                $this->redsys->setParameter("DS_MERCHANT_AMOUNT", $amount);
                $this->redsys->setParameter("DS_MERCHANT_ORDER", $id);
                $this->redsys->setParameter("DS_MERCHANT_MERCHANTCODE", $dataRedsys->redsys_id);
                $this->redsys->setParameter("DS_MERCHANT_CURRENCY", $dataRedsys->redsys_currency);
                $this->redsys->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $trans);
                $this->redsys->setParameter("DS_MERCHANT_TERMINAL", $dataRedsys->redsys_terminal);
                $this->redsys->setParameter("DS_MERCHANT_MERCHANTURL", $url);
                $this->redsys->setParameter("DS_MERCHANT_URLOK", $urlOKKO);
                $this->redsys->setParameter("DS_MERCHANT_PRODUCTDESCRIPTION", $data_operation);
                $this->redsys->setParameter("DS_MERCHANT_URLKO", $urlOKKO);
                $version = self::$hmac_version;
                $kc = $dataRedsys->redsys_key;
                $params = $this->redsys->createMerchantParameters();
                $signature = $this->redsys->createMerchantSignature($kc);

                $table_name = $wpdb->prefix . parent::$DB_PAYMENTS;

                $data_order = array(
                    'time' => $this->date->format('Y-m-d'),
                    'num_order' => $id,
                    'amount' => $amount,
                    'center' => $dataRedsys->Id,
                    'email' => (isset($customer['email'])) ? $customer['email'] : NULL,
                    'name' => (isset($customer['name'])) ? $customer['name'] : NULL,
                    'phone' => (isset($customer['phone'])) ? $customer['phone'] : NULL,
                    'service' => $data_operation,
                    'product_id' => (isset($customer['product_id'])) ? $customer['product_id'] : NULL,
                );

                $wpdb->insert(
                    $table_name,
                    $data_order
                );

                $wpdb->insert_id;

                $form = RedsysTemplate::TemplateButton(
                    $endpoint,
                    $version,
                    $params,
                    $signature
                );

                $response = $form;
            }

            return $response;

        } catch (Exception $e) {
            $r = $e->getMessage();
            error_log("ERROR $r \n", 3, __DIR__ . '/../LOG-ERROR.txt');
            return $e->getMessage();
        }
    }

    public function DecodeData($data = array())
    {
        try {

            $datos = $data["Ds_MerchantParameters"];

            $decodec = $this->redsys->decodeMerchantParameters($datos);
            $decodec = json_decode($decodec, 1);
            $order_id = $decodec['Ds_Order'];
            $order_status = $decodec['Ds_Response'];
            $status = NULL;
            if ((floatval($order_status) >= 0) && (floatval($order_status) < 99)) {
                $status = 'success';
            } else {
                $status = $order_status;
            }

            return array('decode' => $decodec, 'order_id' => $order_id, 'status' => $status);

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}


/*
 * Boton Redsys
 */

class RedsysTemplate
{
    public static function TemplateButton($endpoint = NULL, $version = NULL, $params = NULL, $signature = NULL)
    {

        $style = [
            'width: 100%;',
            'background-color: #28a745;',
            'color: white;',
            'border-radius: .5rem;',
            'overflow: hidden;',
            'font-size: 1.1rem;',
            'padding: .5rem;',
            'height: 3rem;',
            'position: absolute;',
            'left: 0;',
            'top: 10px;'
        ];
        $style = implode('', $style);
        $template = '<body>';
        $template .= '<form name="frm" action="' . $endpoint . '" method="POST" target="_parent">';
        $template .= '<input type="hidden" name="Ds_SignatureVersion" value="' . $version . '"/>';
        $template .= '<input type="hidden" name="Ds_MerchantParameters" value="' . $params . '"/>';
        $template .= '<input type="hidden" name="Ds_Signature" value="' . $signature . '"/>';
        $template .= '<button style="' . $style . '" class="btn btn-success btn-sm btn-block">Pagar</button>';
        $template .= '</form>';
        $template .= '</body>';
        $form = "<iframe id='iframe_redsys' class='pay_iframe' srcdoc='$template'></iframe>";
        return $form;
    }
}