<?php

/**
 * Class FormVithas
 * Author:https://github.com/leifermendez
 */

require_once(__DIR__ . '/SettingField.php');
require_once(__DIR__ . '/RedsysVithas.php');
require_once(__DIR__ . '/SoapVithas.php');
require_once(__DIR__ . '/ParseDown.php');


class FormVithas extends SettingField
{
    protected static $wpdb;
    protected static $product;
    protected static $template;
    protected static $ts_mail_errors;
    protected static $phpmailer;
    protected static $redsys;
    protected static $soap;
    protected static $parse_down;
    protected static $post;
    protected $connection;

    protected function __construct(
        $wpdb = NULL,
        $template = NULL,
        $ts_mail_errors = NULL,
        $phpmailer = NULL,
        $product = NULL,
        $post = NULL
    )
    {
        try {

            self::$wpdb = $wpdb;
            self::$product = $product;
            self::$template = $template;
            self::$ts_mail_errors = $ts_mail_errors;
            self::$phpmailer = $phpmailer;
            self::$post = $post;
            self::$redsys = new RedsysVithas($wpdb);
            self::$soap = new SoapVithas($wpdb);
            self::$parse_down = new Parsedown();

        } catch (Exception $e) {

            return $e->getMessage();
        }
    }

    /*
     * >>>>> FUNCIONES HTML <<<<<
     */

    private function FileInto($raw = null, $file = null)
    {
        try {
            ob_start();
            require($file);
            return ob_get_clean();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function DataMeta($post_id = null, $meta = null)
    {
        try {
            $wpdb = self::$wpdb;
            $table = $wpdb->prefix . parent::$DB_POST_META;
            $table_post = $wpdb->prefix . parent::$DB_POST;

            $fields = array(
                "{$table_post}.ID",
                "{$table_post}.post_title",
                "{$table_post}.post_excerpt",
                "{$table}.meta_value"
            );
            $fields = join(', ', $fields);
            $sql = "SELECT $fields FROM $table INNER JOIN $table_post ON {$table_post}.ID = {$table}.post_id
                WHERE {$table}.post_id = $post_id AND meta_key = '" . $meta . "'";
            $data = $wpdb->get_results($sql);

            return (count($data)) ? $data[0] : null;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function SettingData($key = null)
    {
        try {
            $wpdb = self::$wpdb;
            $table = $wpdb->prefix . parent::$DB_SETTINGS;
            $data = $wpdb->get_results("SELECT * FROM $table WHERE field = '" . $key . "'");
            return (count($data)) ? $data[0] : null;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function SettingUpdate($key = null, $value = null)
    {
        try {
            self::Log('k: ' . $key . ' v:' . $value);
            $wpdb = self::$wpdb;
            $table_name_order = $wpdb->prefix . parent::$DB_SETTINGS;
            $wpdb->update(
                $table_name_order,
                array(
                    'value' => $value
                ),
                array('field' => $key)
            );

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function HeadHtml()
    {
        try {

            $head = '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">';

            echo $head;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function LoadModalHtml()
    {
        try {
            ob_start();
            $field_form = parent::$FIELDS_FORM;
            include(__DIR__ . '/../template/modal/modal-contact.php');
            $html = ob_get_clean();
            echo $html;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function DataPrice($attr = null)
    {
        try {
            $skuData = self::DataMeta($attr, '_sku');
            $productPrice = self::DataMeta($attr, '_price');
            $delimiter = '_';
            $position = 1;
            $agency = explode($delimiter, $skuData->meta_value)[$position];
            $price = ($productPrice) ? $productPrice->meta_value : null;
            $agencyData = self::GetAgency($agency);
            return [
                'index' => $agency,
                'agency' => $agencyData,
                'price' => $price
            ];

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function FormContact($id = null)
    {
        try {

            ob_start();
            $field_form = parent::$FIELDS_FORM;
            $js = file_get_contents(__DIR__ . '/../assets/js/script.js');
            $js_name = 'form_contact_' . time();
            $js = str_replace('mainID', "'#$js_name'", $js);
            $js = str_replace(parent::$MODAL_TRIGGER, '.' . $js_name, $js);
            echo "<script id='_" . $js_name . "' type='text/javascript'>{$js}</script>";
            include(__DIR__ . '/../template/form/form-contact.php');
            $html = ob_get_clean();
            echo $html;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function GetGDRP()
    {
        return self::SettingData('terms_conditions');
    }

    public function GridProductVariationsData($id = null)
    {
        try {
            $variations = array();
            $wpdb = self::$wpdb;
            $table_post = $wpdb->prefix . parent::$DB_POST;
            $table_post_meta = $wpdb->prefix . parent::$DB_POST_META;

            $sql = "
                SELECT p.ID, p.guid, p.post_parent 
                FROM $table_post as p
                 WHERE p.post_parent = $id 
                 AND p.post_type = 'product_variation'
            ";

            $res = $wpdb->get_results($sql);
            $res = (count($res)) ? $res : [];

            foreach ($res as $r) {
                $id_meta = $r->ID;

                $sql_post_meta = "
                    SELECT m.* 
                    FROM $table_post_meta as m where m.post_id = $id_meta and meta_key = '_price'
                 ";

                $sql_post = "
                    SELECT m.ID, m.post_title
                    FROM $table_post as m where m.ID = $id_meta
                ";
//var_dump ($sql_post);
                $tmp = $wpdb->get_results($sql_post_meta);
                $tmp_post = $wpdb->get_results($sql_post);
                $sku = self::DataMeta($id_meta, '_sku');
                $title_name = explode('-', $tmp_post[0]->post_title);
                $title_name = array_reverse($title_name);
                $variations[$id_meta] = array(
                    'ID' => $id_meta,
                    'parent_id' => $id,
                    'name' => (count($tmp_post)) ? $title_name[0] : NULL,
                    'price' => (count($tmp)) ? floatval($tmp[0]->meta_value) : NULL,
                    'sku' => $sku->meta_value,
                    'e' => $tmp_post,
                );

            }

            return $variations;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function GridProductVariations($id = null)
    {
        $data = self::GridProductVariationsData($id);
        $sku_info = array();
        foreach ($data as $datum) {
            $sku_info[$datum['ID']] = self::GetInitForm($datum['ID']);
        }
        ob_start();
        include(__DIR__ . '/../template/grid/product-variations.php');
        $html = ob_get_clean();
        echo $html;

    }

    public function GetInitForm($id = null)
    {
        try {

            $skuData = self::DataMeta($id, '_sku');
            $agency = explode('_', $skuData->meta_value);

            $res = array(
                'center' => (count($agency)) ? $agency[1] : NULL,
                'pay' => (count($agency)) ? ($agency[2] == 1) ? TRUE : FALSE : NULL
            );

            return $res;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function ThankYouPage()
    {
        try {

            $wpdb = self::$wpdb;
            $formContact = parent::$PAGE_CONTACT;
            $formAppointment = parent::$PAGE_APPOINTMENT;
            $formPayment = parent::$PAGE_PAYMENT;

            $pat_tmp = array();
            $path = $_SERVER['REQUEST_URI'];
            $path = explode('/', $path);


            foreach ($path as $p) {
                if (strlen($p) > 0) {
                    $pat_tmp[] = $p;
                }
            }
            $pat_tmp = array_reverse($pat_tmp);
            $trigger = null;

            foreach ($pat_tmp as $pt) {
                if (strpos($pt, $formContact) !== false) {
                    $trigger = 'contact';
                    break;
                } else if (strpos($pt, $formAppointment) !== false) {
                    $trigger = 'appointment';
                    break;
                } else if (strpos($pt, $formPayment) !== false) {
                    $trigger = 'payment';
                    break;
                }
            }

            switch ($trigger) {
                case 'appointment':
                    if (file_exists(get_stylesheet_directory() . '/page-vithas-appointment.php')) {
                        $template = get_stylesheet_directory() . '/page-vithas-appointment.php';
                    } else {
                        $template = __DIR__ . '/../template/thank-you-page/page-vithas-appointment.php';
                    }

                    break;
                case 'contact':
                    if (file_exists(get_stylesheet_directory() . '/page-vithas-contact.php')) {
                        $template = get_stylesheet_directory() . '/page-vithas-contact.php';
                    } else {
                        $template = __DIR__ . '/../template/thank-you-page/page-vithas-contact.php';
                    }
                    break;
                case 'payment':
                    $message_pay = '';
                    $id_order = ($_GET['id']) ? $_GET['id'] : null;
                    $id_order = base64_decode($id_order);

                    $table_name_order = $wpdb->prefix . parent::$DB_PAYMENTS;
                    $table_post = $wpdb->prefix . parent::$DB_POST;

                    $orders = $wpdb->get_results("SELECT * FROM $table_name_order WHERE num_order = $id_order");


                    $fields = array(
                        "{$table_post}.ID",
                        "{$table_post}.post_title"
                    );
                    $fields = join(', ', $fields);

                    $fucCenter = $orders[0]->center;
                    $center = self::$redsys->GetDataRedsys($fucCenter, FALSE);
                    $data_oder = array(
                        'service' => $orders[0]->service,
                        'center' => $center->tsne,
                        'amount' => $orders[0]->amount,
                        'date' => $orders[0]->time,
                        'num_order' => $orders[0]->num_order,
                        'phone' => $orders[0]->phone,
                        'name' => $orders[0]->name,
                        'email' => $orders[0]->email
                    );

                    if (count($orders)) {
                        $sql = "SELECT $fields FROM $table_post WHERE ID = '" . $orders[0]->product_id . "'";
                        $data = $wpdb->get_results($sql);
                        $data_oder['service'] = (!empty($data)) ? $data[0]->post_title : NULL;
                    }


                    if (file_exists(get_stylesheet_directory() . '/page-vithas-payment.php')) {
                        $template = get_stylesheet_directory() . '/page-vithas-payment.php';
                    } else {
                        $template = __DIR__ . '/../template/thank-you-page/page-vithas-payment.php';
                    }

                    break;
                default:
                    $template = get_template_directory() . '/404.php';

                    break;
            }
            require_once($template);

            exit();
        } catch (Exception $e) {
            self::Log('Error: ' . $e->getMessage());
            return $e->getMessage();
        }
    }

    /**
     * @return string
     * >>>>>>ACTIVACION Y BASE DE DATOS <<<<< del plugin para
     */

    public function InstallDB()
    {
        try {
            $wpdb = self::$wpdb;
            $table_name = $wpdb->prefix . parent::$DB_AGENCIES;
            $table_name_order = $wpdb->prefix . parent::$DB_PAYMENTS;
            $table_settings = $wpdb->prefix . parent::$DB_SETTINGS;
            $charset_collate = $wpdb->get_charset_collate();

            self::Log('InstallDB Before SQL');

            $sqlCenters = "CREATE TABLE $table_name (
                id mediumint(9) NOT NULL AUTO_INCREMENT,
                time datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
                tsne tinytext NOT NULL,
                id_attr mediumint NULL,
                district text NULL,
                specialties text NULL,
                type_service text NULL,
                phone tinytext  NULL,
                mail tinytext  NULL,
                mail_cc tinytext  NULL,
                extra tinytext  NULL,
                guid tinytext  NULL,
                text text NULL,
                redsys_id text NULL,
                redsys_terminal text NULL,
                redsys_currency text NULL,
                redsys_env text NULL,
                redsys_key text NULL,
                ws_services text NULL,
                ws_specialties text NULL,
                PRIMARY KEY  (id)
            ) $charset_collate;";

            $sqlPayments = "CREATE TABLE $table_name_order (
                id mediumint(9) NOT NULL AUTO_INCREMENT,
                time datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
                email tinytext NULL,
                num_order tinytext NOT NULL,
                center mediumint(9) NULL,
                service tinytext NULL,
                amount tinytext NULL,
                phone tinytext NULL,
                product_id tinytext NULL,
                name tinytext NULL,
                status tinytext NULL,
                PRIMARY KEY  (id)
            ) $charset_collate;";

            $sqlSettings = "CREATE TABLE $table_settings (
                id mediumint(9) NOT NULL AUTO_INCREMENT,
                field tinytext NULL,
                value text NULL,
                PRIMARY KEY  (id)
            ) $charset_collate;";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

            dbDelta($sqlCenters);
            dbDelta($sqlPayments);
            dbDelta($sqlSettings);

            self::Log('InstallDB After SQL');
            self::InitializedDB();

        } catch (Exception $e) {
            self::Log('Error: ' . $e->getMessage());
            return $e->getMessage();
        }
    }

    private function InsertSetting($key = null, $value = null)
    {
        try {

            $wpdb = self::$wpdb;
            $table_name = $wpdb->prefix . parent::$DB_SETTINGS;

            $wpdb->insert(
                $table_name,
                array(
                    'field' => $key,
                    'value' => $value
                )
            );

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function InitializedDB()
    {
        try {

            $list = array(
                array(
                    'field' => 'version',
                    'value' => '2.0'
                ),
                array(
                    'field' => 'redsys_ws',
                    'value' => 'https://tusalud.vithas.es/wp-json/vithas-form/payment'
                ),
                array(
                    'field' => 'terms_conditions',
                    'value' => 'Mis terminos y condiciones'
                ),
                array(
                    'field' => 'auto_grid',
                    'value' => 'false'
                ),
                array(
                    'field' => 'jquery',
                    'value' => 'false'
                )
            );

            foreach ($list as $value) {
                if (!self::SettingData($value['field'])) {
                    self::InsertSetting($value['field'], $value['value']);
                };
            }

            self::Log('DB Inicializada');

        } catch (Exception $e) {
            self::Log('Error: ' . $e->getMessage());
            return $e->getMessage();
        }
    }

    public function DropTableSetting()
    {
        try {

            $wpdb = self::$wpdb;
            $table = $wpdb->prefix . parent::$DB_SETTINGS;
            $wpdb->query("TRUNCATE $table;");

            self::Log('DB TRUNCATE');

        } catch (Exception $e) {
            self::Log('Error: ' . $e->getMessage());
            return $e->getMessage();
        }
    }

    public function Requirements()
    {
        try {

            $system = array(
                'curl' => (function_exists('curl_version')) ? 'PASS' : 'FAIL',
                'soap' => (extension_loaded('soap')) ? 'PASS' : 'FAIL',
            );

            $style_green = 'color:green;font-weight:bold';
            $style_red = 'color:red;font-weight:bold';

            $html = '<div style="margin-left:0px"  class="error below-h2">';
            $html .= '<b>** Requerimientos minimos VithasForm:</b><br><br>';

            $html .= '<ul>';
            foreach ($system as $k => $v) {
                if ($v === 'PASS') {
                    $html .= '<li><b>' . strtoupper($k) . '</b>: <span style="' . $style_green . '">' . $v . '</span></li>';
                } else {
                    $html .= '<li><b>' . strtoupper($k) . '</b>: <span style="' . $style_red . '">' . $v . '</span></li>';
                }
            }
            $html .= '</ul>';
            $html .= '<br>';
            $html .= '';
            $html .= '<i>Debes instalar las extensiones necesarios para funcionar</i>';
            $html .= '</div>';

            $error = array_search("FAIL", $system);
            return array('html' => $html, 'system' => $error);


        } catch (Exception $e) {
            self::Log('Error: ' . $e->getMessage());
            return $e->getMessage();
        }
    }

    public function DataTerms($key = null)
    {
        try {
            $wpdb = self::$wpdb;
            $table_term_tax = $wpdb->prefix . parent::$DB_TAXONOMIES_WP;
            $sql_tax = "SELECT 1 FROM $table_term_tax where taxonomy = '" . $key . "';";
            $data = $wpdb->get_results($sql_tax);
            $data = (array)$data;

            return (!empty($data)) ? TRUE : FALSE;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function ExtraDataAttr($key = null, $parse = false)
    {
        try {
            $tmp = array();
            $wpdb = self::$wpdb;
            $table = $wpdb->prefix . self::$DB_AGENCIES;
            $sql = "SELECT $key FROM $table";
            $data = $wpdb->get_results($sql);
            $data = (array)$data;
            if (count($data)) {
                foreach ($data as $datum) {
                    $tmp[] = $datum->$key;
                }
            }
            if ($parse) {
                $tmp = implode(',', $tmp);
                $tmp = explode(',', $tmp);
            }

            return array_unique($tmp);

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function InsertTerm($key = null)
    {
        try {

            $wpdb = self::$wpdb;
            $table = $wpdb->prefix . self::$DB_TAXONOMIES_WC;


            $wpdb->insert(
                $table,
                array(
                    'attribute_name' => str_replace('pa_', '', $key),
                    'attribute_label' => ucfirst(str_replace('pa_', '', $key)),
                    'attribute_type' => 'select',
                    'attribute_orderby' => 'menu_order',
                    'attribute_public' => '0'
                )
            );


        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function InsertAttr($tax = null, $value = null)
    {
        try {
            $wpdb = self::$wpdb;
            $table_term = $wpdb->prefix . parent::$DB_TERM;
            $table_tax = $wpdb->prefix . parent::$DB_TAXONOMIES_WP;

            $wpdb->insert(
                $table_term,
                array(
                    'name' => $value,
                    'slug' => strtolower(str_replace([' '], '-', $value))
                )
            );
            $lastid = $wpdb->insert_id;

            $wpdb->insert(
                $table_tax,
                array(
                    'term_id' => $lastid,
                    'taxonomy' => $tax,
                    'description' => $value
                )
            );

            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function CheckTerm()
    {
        try {

            $data = [
                self::$TERM_AGENCIES => self::DataTerms(self::$TERM_AGENCIES),
                self::$TERM_SERVICE => self::DataTerms(self::$TERM_SERVICE),
                self::$TERM_SPECIALTIES => self::DataTerms(self::$TERM_SPECIALTIES),
                self::$TERM_DISTRICT => self::DataTerms(self::$TERM_DISTRICT),
            ];

            foreach ($data as $key => $datum) {
                if (!$datum) {
                    self::InsertTerm($key);
                    if ($key === self::$TERM_DISTRICT) {
                        $d = self::ExtraDataAttr('district');
                        foreach ($d as $dt) {
                            self::InsertAttr(self::$TERM_DISTRICT, $dt);
                        }
                    } elseif ($key === self::$TERM_AGENCIES) {
                        $d = self::ExtraDataAttr('tsne');
                        foreach ($d as $dt) {
                            self::InsertAttr(self::$TERM_AGENCIES, $dt);
                        }
                    } elseif ($key === self::$TERM_SPECIALTIES) {
                        $d = self::ExtraDataAttr('specialties', true);
                        foreach ($d as $dt) {
                            self::InsertAttr(self::$TERM_SPECIALTIES, $dt);
                        }
                    } elseif ($key === self::$TERM_SERVICE) {
                        $d = self::ExtraDataAttr('type_service', true);
                        foreach ($d as $dt) {
                            self::InsertAttr(self::$TERM_SERVICE, $dt);
                        }
                    } elseif ($key === self::$TERM_ID) {
                        $d = self::ExtraDataAttr('id', true);
                        foreach ($d as $dt) {
                            self::InsertAttr(self::$TERM_ID, $dt);
                        }
                    }

                }
            }

            return $data;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function CheckPages()
    {
        try {

            $trigger = false;

            $list = array(
                'Pagina Contacto:' => (file_exists(get_stylesheet_directory() . '/page-vithas-contact.php')) ?
                    TRUE : FALSE,
                'Pagina Pago:' => (file_exists(get_stylesheet_directory() . '/page-vithas-payment')) ?
                    TRUE : FALSE,
                'Pagina Reserva:' => (file_exists(get_stylesheet_directory() . '/page-vithas-appointment.php')) ?
                    TRUE : FALSE,
            );
            $html = '<div style="margin-left:0px" class="error below-h2">';
            $html .= '<b>Paginas</b><br><br>';
            $html .= '<ul>';
            foreach ($list as $k => $v) {
                $t = (!$v) ? ' No encontrado' : ' Econtrado';
                $trigger = (!$trigger && (!$v)) ? true : false;
                $html .= '<li>' . $k . $t . '</li>';
            }
            $html .= '</ul>';
            $html .= '</div>';

            return ($trigger) ? $html : '';

        } catch (Exception $e) {
            self::Log('Error: ' . $e->getMessage());
            return $e->getMessage();
        }
    }

    /*
     * >>>>>REDSYS<<<<<<<
     */

    public function GetRedsysButton($id, $amount, $customer, $redirectTo, $type)
    {
        return self::$redsys->GetRedsysButton($id, $amount, $customer, $redirectTo, $type);
    }

    /*
     * >>>> CONSULTAR DATOS DE AGENCIA <<<<<
     */

    public function GetAgencies($type = NULL, $all = FALSE)
    {
        try {
            $wpdb = self::$wpdb;
            $list = [];
            $table_name = $wpdb->prefix . parent::$DB_AGENCIES;
            $agencies = $wpdb->get_results("SELECT * FROM $table_name");

            foreach ($agencies as $row) {
                $list[] = [
                    'id' => $row->id,
                    'tsne' => $row->tsne,
                    'phone' => $row->phone,
                    'mail' => $row->mail,
                    'mail_cc' => $row->mail_cc,
                    'id_attr' => $row->id_attr,
                    'extra' => $row->extra,
                    'text' => $row->text,
                    'redsys_id' => $row->redsys_id,
                    'redsys_key' => ($all) ? $row->redsys_key : NULL,
                    'redsys_currency' => ($all) ? $row->redsys_currency : NULL,
                    'redsys_terminal' => ($all) ? $row->redsys_terminal : NULL,
                    'redsys_env' => ($all) ? $row->redsys_env : NULL,
                    'district' => ($all) ? $row->district : NULL,
                    'type_service' => ($all) ? $row->type_service : NULL,
                    'specialties' => ($all) ? $row->specialties : NULL,
                ];


            }

            if (!$type) {
                wp_die(json_encode([
                    'data' => $list
                ]));
            } else {
                return $list;
            }


        } catch (Exception $e) {
            self::Log('Error: ' . $e->getMessage());
            return $e->getMessage();
        }
    }

    public function GetAgency($id = null, $by = null)
    {
        try {
            $wpdb = self::$wpdb;
            $table = $wpdb->prefix . parent::$DB_AGENCIES;
            $by = ($by) ? $by : 'id';
            $data = $wpdb->get_results("SELECT * FROM $table WHERE $by ='$id'");
            return (count($data)) ? $data[0] : null;

        } catch (Exception $e) {
            self::Log('Error: ' . $e->getMessage());
            return $e->getMessage();
        }
    }

    public function DeleteAgency($id = null)
    {
        try {
            $wpdb = self::$wpdb;
            $data_agency = self::GetAgency($id);
            $table = $wpdb->prefix . parent::$DB_AGENCIES;
            $table_term = $wpdb->prefix . parent::$DB_TERM;
            $table_term_tax = $wpdb->prefix . parent::$DB_TAXONOMIES_WP;

            $sql = "DELETE FROM $table WHERE (`id` = $id);";
            $sql_term = "DELETE FROM $table_term WHERE `name` = '" . $data_agency->tsne . "';";
            $sql_term_tact = "DELETE FROM $table_term_tax WHERE `description` = '" . $data_agency->tsne . "';";

            $data = $wpdb->get_results($sql);
            $data = $wpdb->get_results($sql_term);
            $data = $wpdb->get_results($sql_term_tact);

            return 'deleted';

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function GetAgenciesCarousel()
    {
        try {
            $wpdb = self::$wpdb;
            $table_name = $wpdb->prefix . parent::$DB_AGENCIES;
            $user = $wpdb->get_results("SELECT * FROM $table_name");

            $html = '<div class="owl-carousel owl-theme">';
            foreach ($user as $row) {
                $html .= '<div class="centro-item">';
                $html .= '<h5>' . $row->tsne . '</h5>';
                $html .= '<p>' . $row->text . '<br>';
                $html .= 'Email: ' . $row->mail . '<br>';
                $html .= 'Teléfono: ' . $row->phone . '</p>';
                $html .= '<a itemprop="url" href="' . $row->extra . '" target="_self" class="mkdf-btn mkdf-btn-large mkdf-btn-solid mkdf-btn-solid-dark mkdf-btn-icon">';
                $html .= ' <span class="mkdf-btn-text">Ver centro</span>';
                $html .= '<span class="mkdf-btn-icon-holder">';
                $html .= '<span class="mkdf-btn-icon-normal"><i class="mkdf-icon-ion-icon ion-chevron-right "></i></span>';
                $html .= '<span class="mkdf-btn-icon-flip"><i class="mkdf-icon-ion-icon ion-chevron-right "></i></span>';
                $html .= '</span></a>';
                $html .= '</div>';
            }
            $html .= '<div class="mkdf-tes-nav mkdf-tes-controls dark">';
            $html .= '<span class="mkdf-tes-nav-prev">';
            $html .= '<span class="mkdf-icon-mark">';
            $html .= '</span>';
            $html .= '</span>';
            $html .= '<span class="mkdf-tes-nav-next">';
            $html .= '<span class="mkdf-icon-mark">';
            $html .= '</span>';
            $html .= '</span>';
            $html .= '</div>';
            $html .= '</div>';

            return $html;

        } catch (Exception $e) {
            self::Log('Error: ' . $e->getMessage());
            return $e->getMessage();
        }
    }


    /**
     * @param $data
     * @return array|string
     * >>>>> DISPARAR MAIL <<<<<
     */

    public function MailContact(
        $message = '',
        $email = '',
        $subject = null,
        $header = null,
        $dataUser = array(),
        $soap = false
    )
    {
        try {
            $response = array(
                'prm' => null,
                'mail' => null
            );

            $header = array(
                'Content-Type: ' . parent::$MAIL_ENCODE,
                'From: ' . parent::$MAIL_FROM
            );

            $inDataUser = array(
                'name' => '',
                'last_name' => '',
                'phone' => '',
                'email' => '',
                'centro' => '', //20
            );

            $dataUser = array_merge($inDataUser, $dataUser);

            $message = $this->FileInto($message, parent::$MAIL_TEMPLATE);

            wp_mail($email, $subject, $message, $header);

            $ts_mail_errors = self::$ts_mail_errors;
            $phpmailer = self::$phpmailer;

            if (!isset($ts_mail_errors)) $ts_mail_errors = array();

            if (isset($phpmailer)) {
                $ts_mail_errors[] = $phpmailer->ErrorInfo;
            }

            $senderMail = (!count($ts_mail_errors)) ? 'not_error' : 'error';
            $response['mail'] = $senderMail;

            $result = self::GetAgency($dataUser['centro'],'tsne');
            self::Log('SOAP Antes: ' . $result->id_attr);

            if ($soap && $result) {
                $dataUser['centro'] = $result->id_attr;
                $idSend = self::$soap->Send($dataUser);
                $response['prm'] = $idSend;
            }
            $response['extra_data'] = json_encode($dataUser);
            self::Log('Mail Enviado: (' . json_encode($dataUser) . ') ' . (gettype($email) === 'array') ? json_encode($email) : $email);
            return $response;

        } catch (Exception $e) {
            self::Log('Error: ' . $e->getMessage());
            return $e->getMessage();
        }
    }


    /**
     * @param $data
     * @return array|string
     * >>>>> WEBHOOK <<<<<
     */

    public function WebHook($data)
    {
        try {

            $wpdb = self::$wpdb;

            $table_name_order = $wpdb->prefix . static::$DB_PAYMENTS;
            $table_post = $wpdb->prefix . parent::$DB_POST;

            $fields = array(
                "{$table_post}.ID",
                "{$table_post}.post_title"
            );
            $fields = join(', ', $fields);

            $parameters = json_decode(json_encode($data), 1);
            $decodeData = self::$redsys->DecodeData($parameters);
            $wpdb->update(
                $table_name_order,
                array(
                    'status' => $decodeData['status']
                ),
                array('num_order' => $decodeData['order_id'])
            );

            $orders = $wpdb->get_results("
                SELECT * FROM $table_name_order 
                WHERE num_order =" . $decodeData['order_id']);

            $dataUser = array(
                'name' => (count($orders)) ? $orders[0]->name : NULL,
                'last_name' => '',
                'phone' => (count($orders)) ? $orders[0]->phone : NULL,
                'email' => (count($orders)) ? $orders[0]->email : NULL,
                'centro' => (count($orders)) ? $orders[0]->center : NULL,
                'product_id' => (count($orders)) ? $orders[0]->product_id : NULL,
                'product_title' => null,
            );

            if (count($orders)) {
                $sql = "SELECT $fields FROM $table_post WHERE ID = '" . $dataUser['product_id'] . "'";
                $data = $wpdb->get_results($sql);
                $dataUser['product_title'] = (!empty($data)) ? $data[0]->post_title : NULL;
            }


            $center = self::GetAgency($dataUser['centro']);

            $mails = [
                $dataUser['email'],
                $center->mail,
                $center->mail_cc
            ];


            $email = $mails;
            $subject = parent::$MAIL_PAY_SUBJECT;

            $raw = [
                'Nombre' => $dataUser['name'],
                'Teléfono' => $dataUser['phone'],
                'Email' => $dataUser['email'],
                'Centro' => $center->tsne,
            ];

            $amount = (count($orders)) ? $orders[0]->amount : 0;
            $amount = floatval($amount / 100);

            $date = (count($orders)) ? $orders[0]->time : null;
            $subject .= "[{$center->tsne}][{$decodeData['order_id']}]";
            $message_pay = [
                "¡Gracias por tu compra",
                '',
                "Hola {$dataUser['name']}",
                "Éste es el resumen de tu compra",
                "Servicio: {$dataUser['product_title']}",
                "Centro: {$center->tsne}",
                "Importe: {$amount} EUR",
                "Número de referencia: {$decodeData['order_id']}",
                "Fecha de compra: {$date}",
                "",
                'En menos de 24 horas, un asesor personal de salud se pondrá en contacto contigo para concretar los detalles de tu cita. '
            ];


            /* ob_start();
             require(__DIR__ . '/../template/email/email-template.php');
             $message_pay = ob_get_clean();*/

            self::MailContact($message_pay, $email, $subject, null, $dataUser, true);
            self::Log('Mail Payment Send');
            return $decodeData;

        } catch (Exception $e) {
            self::Log('Error: ' . $e->getMessage());
            return $e->getMessage();
        }
    }

    /**
     * @return string
     * >>>>> PANEL DE ADMINISTACION DEL PLUGIN <<<<<
     */

    public function AdminPage()
    {
        try {
            require_once(__DIR__ . '/../template/admin/main-menu.php');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function AdminPageList()
    {
        try {
            if (($_GET['page'] === 'vithas-list-centros')
                && ($_GET['delete'] === 'true')
                && ($_GET['id'])
            ) {
                self::DeleteAgency($_GET['id']);
            }
            $data = self::GetAgencies(TRUE, TRUE);
            ob_start();
            require_once(__DIR__ . '/../template/admin/list-centers.php');
            $html = ob_get_clean();
            echo $html;


        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function AdminPageInside()
    {
        try {

            $agencyData = self::GetAgency((isset($_GET['Id'])) ? $_GET['Id'] : NULL);

            $data = array(
                'Id' => (isset($_GET['Id'])) ? $_GET['Id'] : NULL,
                'tsne' => (isset($agencyData)) ? $agencyData->tsne : NULL,
                'id_attr' => (isset($agencyData)) ? $agencyData->id_attr : NULL,
                'phone' => (isset($agencyData)) ? $agencyData->phone : NULL,
                'mail' => (isset($agencyData)) ? $agencyData->mail : NULL,
                'mail_cc' => (isset($agencyData)) ? $agencyData->mail_cc : NULL,
                'extra' => (isset($agencyData)) ? $agencyData->extra : NULL,
                'redsys_id' => (isset($agencyData)) ? $agencyData->redsys_id : NULL,
                'redsys_terminal' => (isset($agencyData)) ? $agencyData->redsys_terminal : NULL,
                'redsys_currency' => (isset($agencyData)) ? $agencyData->redsys_currency : NULL,
                'redsys_key' => (isset($agencyData)) ? $agencyData->redsys_key : NULL,
                'redsys_env' => (isset($agencyData)) ? $agencyData->redsys_env : NULL,
                'district' => (isset($agencyData)) ? $agencyData->district : NULL,
                'specialties' => (isset($agencyData)) ? $agencyData->specialties : NULL,
                'type_service' => (isset($agencyData)) ? $agencyData->type_service : NULL,
            );

            require_once(__DIR__ . '/../template/admin/add-center.php');

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function AdminDocumentation()
    {
        try {
            $mark = file_get_contents(__DIR__ . '/../README.md');
            $mark = self::$parse_down->text($mark);
            echo $mark;
        } catch (Exception $e) {
            self::Log('Error: ' . $e->getMessage());
            return $e->getMessage();
        }
    }

    public function AdminSettings()
    {
        try {
            $setting = array(
                'version' => self::SettingData('version'),
                'redsys_ws' => self::SettingData('redsys_ws'),
                'terms_conditions' => self::SettingData('terms_conditions'),
                'auto_grid' => self::SettingData('auto_grid'),
                'jquery' => self::SettingData('jquery')
            );
            require_once(__DIR__ . '/../template/admin/settings.php');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function SaveSettings()
    {
        try {

            $data = $_POST['fields'];

            foreach ($data as $key => $value) {
                self::SettingUpdate($key, $value);
            }
            echo json_encode(['status' => 'success']);
            wp_die();

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function SaveCenter()
    {
        try {

            $wpdb = self::$wpdb;
            $table_name = $wpdb->prefix . parent::$DB_AGENCIES;
            $table_term = $wpdb->prefix . parent::$DB_TERM;
            $table_tax = $wpdb->prefix . parent::$DB_TAXONOMIES_WP;

            if ($_POST['id']) {

                $tmp_name = self::GetAgency($_POST['id']);

                $wpdb->update(
                    $table_term,
                    array(
                        'name' => $_POST['tsne'],
                        'slug' => strtolower(str_replace([' '], '-', $_POST['tsne']))
                    ),
                    array('name' => $tmp_name->tsne)
                );

                $wpdb->update(
                    $table_tax,
                    array(
                        'description' => $_POST['tsne']
                    ),
                    array('description' => $tmp_name->tsne)
                );

                $wpdb->update(
                    $table_name,
                    array(
                        'time' => current_time('mysql'),
                        'tsne' => $_POST['tsne'],
                        'phone' => $_POST['phone'],
                        'mail' => $_POST['mail'],
                        'mail_cc' => $_POST['mail_cc'],
                        'text' => $_POST['text'],
                        'extra' => $_POST['extra'],
                        'id_attr' => $_POST['id_attr'],
                        'redsys_id' => $_POST['redsys_id'],
                        'redsys_terminal' => $_POST['redsys_terminal'],
                        'redsys_currency' => $_POST['redsys_currency'],
                        'redsys_env' => $_POST['redsys_env'],
                        'redsys_key' => $_POST['redsys_key'],
                        'district' => $_POST['district'],
                        'specialties' => $_POST['specialties'],
                        'type_service' => $_POST['type_service']
                    ),
                    array('id' => $_POST['id'])
                );

            } else {

                $wpdb->insert(
                    $table_term,
                    array(
                        'name' => $_POST['tsne'],
                        'slug' => strtolower(str_replace([' '], '-', $_POST['tsne']))
                    )
                );
                $lastid = $wpdb->insert_id;

                $wpdb->insert(
                    $table_tax,
                    array(
                        'term_id' => $lastid,
                        'taxonomy' => 'pa_centros',
                        'description' => $_POST['tsne']
                    )
                );

                $wpdb->insert(
                    $table_name,
                    array(
                        'time' => current_time('mysql'),
                        'tsne' => $_POST['tsne'],
                        'phone' => $_POST['phone'],
                        'mail' => $_POST['mail'],
                        'mail_cc' => $_POST['mail_cc'],
                        'text' => $_POST['text'],
                        'extra' => $_POST['extra'],
                        'id_attr' => $_POST['id_attr'],
                        'redsys_id' => $_POST['redsys_id'],
                        'redsys_terminal' => $_POST['redsys_terminal'],
                        'redsys_currency' => $_POST['redsys_currency'],
                        'redsys_env' => $_POST['redsys_env'],
                        'redsys_key' => $_POST['redsys_key'],
                        'district' => $_POST['district'],
                        'specialties' => $_POST['specialties'],
                        'type_service' => $_POST['type_service']
                    )
                );


            }

            echo json_encode(['status' => 'success']);
            wp_die();

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function NoticeSku()
    {
        try {

            $agencies = self::GetAgencies(TRUE);

            $style = [
                'padding: 1rem',
                'border: solid 1px'
            ];
            $style = implode(';', $style);
            $html = "<div style='" . $style . "' class='form-row form-row-full options alert alert-warning'>";
            $html .= "<div><b>ATENCION!</b></div>";
            $html .= "<p>Recuerda la nomenclatura de los SKU. A_B_C <a href='/wp-admin/admin.php?page=vithas-documentation'>Ver más</a></p> ";
            $html .= "<p id='example_sku_vh'>1: Comprar + Reservar<br>2: Reservar</p>";
            $html .= "<div><select id='sku_select_vh'>";
            $html .= "<option>Seleccionar Centro</option>";
            foreach ($agencies as $agency) {
                $html .= "<option value='" . $agency['id'] . "'>" . $agency['tsne'] . "</option>";
            }
            $html .= "</select></div>";
            $html .= "</div>";
            echo $html;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function GetHospitals()
    {
        return self::$soap->GetHospitals(true);
    }

    public function Log($message = null)
    {
        try {

            error_log($message . " \n", 3, __DIR__ . '/../LOG-ERROR.txt');

        } catch
        (Exception $e) {
            return $e->getMessage();
        }
    }
}