<?php

/*
 * Clase para conectar con el PRM
 */

require_once(__DIR__ . '/../requires/soap/ntml.php');
require_once(__DIR__ . '/SOAPFields.php');
require_once(__DIR__ . '/SettingField.php');

class SoapVithas extends SOAPFields
{

    private static $soap_user = 'vithas\IntegracionTSNE';
    private static $soap_pass = 'B8P7GnR*15';

    private static $wpdb;

    public function __construct($wpdb)
    {
        self::$wpdb = $wpdb;
    }

    private function Connect()
    {
        try {

            define('USERPWD', self::$soap_user . ':' . self::$soap_pass);
            stream_wrapper_unregister("https");
            stream_wrapper_register("https", "NTLMStream");

            $params = [
                'stream_context' => stream_context_create([
                    'ssl' => [
                        'ciphers' => 'RC4-SHA',
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true,
                    ]
                ]),
                'cache_wsdl' => WSDL_CACHE_NONE,
                'soap_version' => SOAP_1_1,
                'trace' => 1,
                'connection_timeout' => 180,
                'features' => SOAP_SINGLE_ELEMENT_ARRAYS
            ];

            $client = new NTLMSoapClient(
                SOAPFields::$SERVICE_FORM_CONTACT,
                $params);

            return $client;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function GetAgencyByKey($key = null, $value = null)
    {
        try {
            $wpdb = self::$wpdb;
            $table = $wpdb->prefix . SettingField::$DB_AGENCIES;
            $sql = "SELECT * FROM $table WHERE {$key} = {$value}";
            $data = $wpdb->get_results($sql);
            return (count($data)) ? $data[0] : null;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    private function AddAgency($value = null)
    {
        try {
            $wpdb = self::$wpdb;
            $table = $wpdb->prefix . SettingField::$DB_AGENCIES;
//
//            id
//            time
//            tsne
//            id_attr
//            district
//            specialties
//            type_service
//            phone
//            mail
//            mail_cc
//            extra
//            text
//            redsys_id
//            redsys_terminal
//            redsys_currency
//            redsys_env
//            redsys_key

            $array = array (
                'guid' => $value->vit_hospitalid,
                'tsne' => $value->vit_name,
                'mail' => $value->vit_email,
                'phone' => $value->vit_telefono,
                'id_attr' => $value->vit_identificador,
                'district'=> $value->vit_provincia,
            );
            $data = $wpdb->insert($table ,$array);
            return (count($data)) ? $data[0] : null;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    // $table = $wpdb->prefix.'you_table_name';

    private function SetAgencyByKey($id = null, $key = null, $value = null)
    {
        try {
            $wpdb = self::$wpdb;
            $table = $wpdb->prefix . SettingField::$DB_AGENCIES;
            $data = $wpdb->update($table, array($key => $value), array('id' => $id));

            return (!empty($data)) ? $data : null;
        } catch (Exception $e) {

            return $e->getMessage();
        }
    }

    public function Send($dataUser)
    {

        try {

            $client = $this->Connect();

            $client = $client->FormularioDeContacto([
                'origen' => 'PRM',
                'nombre' => $dataUser['name'],
                'apellidos' => $dataUser['last_name_1'],
                'telefono' => $dataUser['phone'],
                'email' => $dataUser['email'],
                'idHospital' => $dataUser['centro'],
                'lopd' => 'ConsentimientoAceptado',
                'pub' => 'ConsentimientoAceptado',
                'gdprConsFamiliar' => 'Aceptado',
                'gdprConsAseguradora' => 'Aceptado'
            ]);

            return $client->FormularioDeContactoResult;

        } catch (SoapFault $e) {
            return false;
        }
    }

    public function GetHospitals($sync = false)
    {
        try {
            $client = $this->Connect();

            $client = $client->ListaHospitales();
            $client = (array)$client;

            $list = $client['ListaHospitalesResult']->InfoHospital;
            if (!empty($list)) {
                if ($sync) {
                    foreach ($list as $k => $v) {
                        $list_services = array();
                        $res = $this->GetAgencyByKey('id_attr', $v->vit_identificador);

                        if ($res) {
                            $this->SetAgencyByKey($res->id, 'guid', $v->vit_hospitalid);
                            $services = $this->GetElements($v->vit_hospitalid);

                            foreach ($services as $service) {
                                $list_services[] = array(
                                    'vit_name' => $service->vit_name,
                                    'vit_guid' => $service->Id,
                                    'vit_identificadorprm' => $service->vit_identificadorprm
                                );
                            }

                            if (count($list_services)) {
                                $this->SetAgencyByKey($res->id, 'ws_services', json_encode($list_services));
                            }

                            $ws_specialties = array();

                            foreach ($list_services as $sv) {
                                $ws_specialties['Servicio'][$sv['vit_name']] = $this->GetElements(
                                    $v->vit_hospitalid,
                                    'Servicio',
                                    6,
                                    $sv['vit_guid']
                                );

                                /*
                                 * TODO Me devuelve null
                                 * $ws_specialties['Producto'][$sv['vit_name']] = $this->GetElements(
                                    $v->vit_hospitalid,
                                    'Producto',
                                    6,
                                    $sv['vit_guid']
                                );*/
                            }

                            if (count($ws_specialties)) {
                                $this->SetAgencyByKey($res->id, 'ws_specialties', json_encode($ws_specialties));
                            }
                        } else {;
                           $res =  $this->AddAgency($v);
                        }

                    }
                }
            }

            return $list;

        } catch (SoapFault $e) {
            return $e->getMessage();
        }
    }

    public function GetProductLine()
    {
        try {

            $client = $this->Connect();

            $client = $client->LineaProducto([
                'Hospital' => '9fbfa990-7908-e511-80d6-00155d000b5c'
            ]);

            var_dump($client);

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function GetElements($hospital = NULL,
                                $type = 'TipoServicio',
                                $product_line = 6,
                                $parent = NULL, $child = NULL)
    {
        try {
            /*
             *    <nivelPrm>TipoServicio or Servicio or Producto</nivelPrm>
             *    <padre>guid</padre>
             *    <abuelo>guid</abuelo>
             * 9fbfa990-7908-e511-80d6-00155d000b5c
             * Example: Cirugia (10000)
             * 13bd331c-0595-e611-80ee-00155d0a7c6b
             */

            $client = $this->Connect();

            $client = $client->ElementoCatalogo([
                'Hospital' => $hospital,
                'LineaProducto' => $product_line,
                'nivelPrm' => $type,
                'padre' => $parent
            ]);

            $result = (array)$client;
            $result = $result['ElementoCatalogoResult']->InfoElementoCatalogo;

            return $result;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}