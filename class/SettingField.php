<?php

class SettingField
{

    public static $MODAL_TRIGGER = '.v2-vh-modal';
    /**
     * Nombre tabla en base de datos
     * @var string
     */

    public static $DB_AGENCIES = 'vithas_agencies';
    public static $DB_PAYMENTS = 'vithas_user_payments';
    public static $DB_SETTINGS = 'vithas_settings';
    public static $DB_TAXONOMIES_WC = 'woocommerce_attribute_taxonomies';
    public static $DB_TAXONOMIES_WP = 'term_taxonomy';
    public static $DB_TERM = 'terms';
    public static $DB_TERMS_RELATIONSHIP = 'term_relationships';
    public static $DB_POST_META = 'postmeta';
    public static $DB_POST = 'posts';

    public static $TERM_AGENCIES = 'pa_centros';
    public static $TERM_DISTRICT = 'pa_provincia';
    public static $TERM_SPECIALTIES = 'pa_especialidad';
    public static $TERM_SERVICE = 'pa_servicio';

    /**
     * Mail
     */

    public static $MAIL_FROM = 'Vithas.es <no-reply@vithas.es>';
    public static $MAIL_ENCODE = 'text/html; charset=UTF-8';
    public static $MAIL_TEMPLATE = __DIR__ . '/../template/email/email-template.php';

    public static $MAIL_PAY_SUBJECT = '¡Gracias por tu compra!';
    public static $MAIL_CONTACT_SUBJECT = '¡Gracias por ponerte en contacto con nosotros!';
    public static $MAIL_CONTACT_APPOINTMENT = 'Nueva solicitud de cita';


    /**
     * FieldS Form
     */

    public static $FIELDS_FORM = array(
        [
            'label' => 'Nombre',
            'name' => 'name',
            'id' => 'name',
            'type' => 'input'
        ],
        [
            'label' => 'Primer apellido',
            'name' => 'last_name_1',
            'id' => 'last_name_1',
            'type' => 'input'
        ],
        [
            'label' => 'Seundo Apellido',
            'name' => 'last_name_2',
            'id' => 'last_name_2',
            'type' => 'input'
        ],
        [
            'label' => 'Teléfono',
            'name' => 'phone',
            'id' => 'phone',
            'type' => 'input'
        ],
        [
            'label' => 'Email',
            'name' => 'email',
            'id' => 'email',
            'type' => 'input'
        ],
        [
            'label' => 'Repetir email',
            'name' => 'repetir_email',
            'id' => 'repetir_email',
            'type' => 'input'
        ],
        [
            'label' => 'Centro',
            'name' => 'centro',
            'id' => 'centro',
            'type' => 'select'
        ]
    );

    /**
     * ThankYou page
     * @param null $template
     */


    public static $PAGE_PAYMENT = 'compra_confirmada';
    public static $PAGE_APPOINTMENT = 'cita_confirmada';
    public static $PAGE_CONTACT = 'formulario_confirmado';


}