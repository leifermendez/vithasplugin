<?php
/**
 * Class FiltersVithas
 * Author:https://github.com/leifermendez
 */

class FiltersVithas extends FormVithas
{
    public function LoadHtmlFilter($data = array())
    {
        try {
      
            ob_start();
            include(__DIR__ . '/../template/filters/filter-grid.php');
            $html = ob_get_clean();
            return $html;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function GetFilters($opt = array())
    {
        try {

            $wpdb = parent::$wpdb;
            $table = $wpdb->prefix . parent::$DB_TAXONOMIES_WC;
            $table_term = $wpdb->prefix . parent::$DB_TERM;
            $table_term_tax = $wpdb->prefix . parent::$DB_TAXONOMIES_WP;
            $data = $wpdb->get_results("SELECT * FROM $table ORDER BY attribute_id ASC");

            $data = (array)$data;

            $html = array();

            foreach ($data as $datum) {

                $tax = "pa_" . $datum->attribute_name;

                $sql_inside = "
                    SELECT wp_terms.name as value,
                    wp_terms.term_id,
                    wp_term_tax.taxonomy
                     FROM $table_term_tax as wp_term_tax 
                    INNER JOIN $table_term as wp_terms on
                    wp_term_tax.term_id = wp_terms.term_id
                    WHERE wp_term_tax.taxonomy = '" . $tax . "'
                ";

                $data_term = $wpdb->get_results($sql_inside);
                $data_term = (array)$data_term;

                $html[] = [
                    'tax' => $tax,
                    'attribute_label' => $datum->attribute_label,
                    'attribute_name' => $datum->attribute_name,
                    'attribute_child' => $data_term
                ];

            }

            $response = $html;

            ob_start();
            include(__DIR__ . '/../template/filters/filter-select.php');
            $html = ob_get_clean();

            return $html;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function GetProductByAttr($attr = array())
    {
        try {
            $raw = array();
            $response = array();
            foreach ($attr as $key => $value) {
                $tmp = self::GetProductTax($key, $value);
                foreach ($tmp as $value) {
                    $raw[$value->object_id][] = (array)$value;
                }
            }

        
       

            foreach ($raw as $value) {
           // var_dump(count($value).'----'.count($attr));
          
            
               if (count($value) === count($attr)) {
                    $id_post_meta = NULL;
                    $url = NULL;
                    if (isset($value[0]['object_id'])) {
                        $id_post_meta = $value[0]['object_id'];
                    } else {
                        $id_post_meta = $value[0][0]['object_id'];
                    }

            		$url = wp_get_attachment_image_src( get_post_thumbnail_id($id_post_meta), 'medium')[0];
 
                    //$data_meta_key_parent = parent::DataMeta($id_post_meta, '_thumbnail_id');
                    /*if (!empty($data_meta_key_parent)) {
                        $data_meta_key_parent = parent::DataMeta($data_meta_key_parent->meta_value,
                            '_wp_attached_file');

                        if (!empty($data_meta_key_parent)) {
                            $url = content_url()."/uploads/".$data_meta_key_parent->meta_value;
                        }
                    }*/

                    $price = $_product = wc_get_product($value[0]['object_id']);
                    $single = $value[0];
                    $single['attached'] = $url;
                    $single['prices'] = [
                        'regular_price' => $price->get_regular_price(),
                        'sale_price' => $price->get_sale_price(),
                        'price' => $price->get_price()
                    ];
                    $response[] = $single;
                }
            }

            return $response;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }


    private function insideParse($key = null, $value = null)
    {
        try {
            $sql = '';
            switch ($key) {
                case 'provincia':
                    $sql .= " wp_tax.taxonomy = 'pa_" . $key . "' and wp_term.name LIKE '%" . $value . "%' ";
                    break;
                case 'centros':
                    $sql .= " wp_tax.taxonomy = 'pa_" . $key . "' and wp_tax.description LIKE '%" . $value . "%' ";
                    break;
                case 'especialidad':
                    $sql .= " wp_relation.term_taxonomy_id  = $value";
                    break;
               case 'servicio':
                  $sql .= " wp_tax.taxonomy = 'pa_" . $key . "' and wp_tax.description LIKE '%" . $value . "%' ";
                    break;

            }
        
            return $sql;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function GetProductTax($key, $value)
    {
        try {

            $wpdb = parent::$wpdb;
            $table_terms = $table = $wpdb->prefix . parent::$DB_TERM;
            $table_text_tax = $table = $wpdb->prefix . parent::$DB_TAXONOMIES_WP;
            $table_relationship = $table = $wpdb->prefix . parent::$DB_TERMS_RELATIONSHIP;
            $table_post = $table = $wpdb->prefix . parent::$DB_POST;

            $sql = "
            SELECT wp_tax.*, wp_term.name as term_name,  
            wp_relation.object_id, wp_posts.post_title,
            wp_posts.guid
            FROM $table_text_tax as wp_tax
            INNER join $table_terms as wp_term
            on wp_tax.term_id = wp_term.term_id
            INNER JOIN $table_relationship as wp_relation
            on wp_relation.term_taxonomy_id = wp_tax.term_taxonomy_id
            INNER JOIN $table_post as wp_posts
            on wp_posts.ID = wp_relation.object_id
             where (".$this->insideParse($key, $value).")
            ";

            /*
             *    where (
                wp_tax.taxonomy = 'pa_" . $key . "'
                and wp_relation.term_taxonomy_id  = $value
            )
             * */

            $data = $wpdb->get_results($sql);
            return (!empty($data)) ? $data : [];

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function DataFilter($filters = null)
    {
        $filters = (isset($filters)) ? $filters : [];
        $filters = explode('|', $filters);
        $fil = array();
        foreach ($filters as $value) {
            $tmp = explode('=', $value);
            if (isset($tmp[0]) && isset($tmp[1])) {
                $fil[$tmp[0]] = $tmp[1];
            }

        }
        $result = self::GetProductByAttr($fil);
        return $result;
    }

    public function CentersBy($key = null, $value = null)
    {
        try {
            $wpdb = self::$wpdb;
            $table = $wpdb->prefix . parent::$DB_AGENCIES;
            $value = "'%" . $value . "%'";
            $sql_inside = "
                SELECT * FROM $table
                where $key  LIKE $value
            ";
            $data_term = $wpdb->get_results($sql_inside);
            $data_term = (array)$data_term;

            return $data_term;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function Serialize($res, $key = null)
    {
        try {

            $filters = (isset($res)) ? $res : [];
            $filters = explode('|', $filters);
            $fil = array();
            foreach ($filters as $value) {
                $tmp = explode('=', $value);
                if (isset($tmp[0]) && isset($tmp[1]) && ($tmp[0] === $key)) {
                    $fil[$key] = $tmp[1];
                }

            }

            return (isset($fil[$key])) ? $fil[$key] : null;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function GetAllByTerm($key = null, $term = null)
    {
        try {
            $wpdb = self::$wpdb;
            $table = $wpdb->prefix . parent::$DB_TAXONOMIES_WP;
            $sql = "SELECT * FROM $table where $key ='pa_" . $term . "'";
            $data_term = $wpdb->get_results($sql);
            $data_term = (array)$data_term;
            return $data_term;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}