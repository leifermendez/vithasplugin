<div class="form-group col-12">
    <div class="checkbox">
        <label class="datos_protect"><input id="datos_protect" type="checkbox" required>
            He leído y acepto la información básica sobre <a
                    href="https://tusalud.vithas.es/politica-de-proteccion-de-datos" class="datos_protect">Protección de
                datos</a> aplicable, <a href="http://tusalud.vithas.es/aviso-legal" target="_blank">Aviso
                Legal</a> y <a href="https://tusalud.vithas.es/politica-de-cookies" target="_blank">Política
                de uso</a> del sitio.
        </label>
    </div>
</div>
<div class="form-group col-12">
    <div class="checkbox">
        <label><input type="checkbox">
            Acepto ser informado sobre servicios sanitarios de VITHAS</label>
    </div>
</div>