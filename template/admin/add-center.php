<?php
$data = (isset($data)) ? $data : [];
?>
<div class="container">
    <form id="add-form">
        <h4>Centro</h4>
        <input type="hidden" class="form-control" value="<?php echo $data['id'] ?>" id="id">
        <div class="form-group">
            <label for="tsne">Nombre:</label>
            <input type="text" class="form-control" value="<?php echo $data['tsne'] ?>" id="tsne">
        </div>
        <div class="form-group">
            <label for="id_attr">ID PRM:</label>
            <input type="text" class="form-control" value="<?php echo $data['id_attr'] ?>" id="id_attr">
        </div>
        <div class="form-group">
            <label for="phone">Telfono:</label>
            <input type="text" class="form-control" value="<?php echo $data['phone'] ?>" id="phone">
        </div>
        <div class="form-group">
            <label for="district">Provincia:</label>
            <input type="text" class="form-control" value="<?php echo $data['district'] ?>" id="district">
        </div>
        <div class="form-group">
            <label for="mail">Mail principal:</label>
            <input type="text" class="form-control" value="<?php echo $data['mail'] ?>" id="mail">
        </div>
        <div class="form-group">
            <label for="mail_cc">Mail copias:</label>
            <input type="text" class="form-control" value="<?php echo $data['mail_cc'] ?>" id="mail_cc">
        </div>

        <div class="form-group">
            <label for="redsys_id">Redsys ID:</label>
            <input type="text" class="form-control" value="<?php echo $data['redsys_id'] ?>" id="redsys_id">
        </div>

        <div class="form-group">
            <label for="redsys_key">Redsys KEY:</label>
            <input type="text" class="form-control" value="<?php echo $data['redsys_key'] ?>" id="redsys_key">
        </div>

        <div class="form-group">
            <label for="redsys_terminal">Redsys Terminal:</label>
            <input type="text" class="form-control" value="<?php echo $data['redsys_terminal'] ?>" id="redsys_terminal">
        </div>

        <div class="form-group">
            <label for="redsys_currency">Redsys Divisa:</label>
            <input type="text" class="form-control" value="<?php echo $data['redsys_currency'] ?>" id="redsys_currency">
        </div>

        <div class="form-group">
            <label for="redsys_env">Redsys Entorno:</label>
            <input type="text" class="form-control" value="<?php echo $data['redsys_env'] ?>" id="redsys_env">
        </div>

        <div class="form-group">
            <label for="specialties">Especialidades:</label>
            <input type="text" class="form-control" value="<?php echo $data['specialties'] ?>" id="specialties">
        </div>

        <div class="form-group">
            <label for="type_service">Especialidades:</label>
            <input type="text" class="form-control" value="<?php echo $data['type_service'] ?>" id="type_service">
        </div>

        <div class="form-group">
            <label for="extra">Extra:</label>
            <input type="text" class="form-control" value="<?php echo $data['extra'] ?>" id="extra">
        </div>

        <button type="button" onClick="_send()" class="btn btn-default">Submit</button>
    </form>
</div>