<?php
$data = (isset($data)) ? $data : '';
?>
<div class="wrap">
    <div id="post-body-content" class="edit-form-section edit-comment-section">
        <?php echo $data; ?>
    </div>
</div>