<?php
$data = (isset($data)) ? $data : [];

?>
<style>
    .wp-list-table th, .wp-list-table td {
        font-size: 10px;
    }
</style>
<div style="margin-top: 20px">
    <a href="/wp-admin/admin.php?page=vithas-add-centros" style="margin-bottom: 10px" type="button"
       class="button">Add</a>
    <table class="wp-list-table widefat fixed striped posts">
        <thead>
        <tr>
            <th scope="col" id="center_name">Nombre</th>
            <th scope="col" style="width:25px" id="center_attr">PRM</th>
            <th scope="col" id="center_phone">Teléfono</th>
            <th scope="col" id="center_phone">Provincia</th>
            <th scope="col" id="center_mail">Mail principal</th>
            <th scope="col" id="center_mail_cc">Mail Copias</th>
            <th scope="col" id="center_redsysfuc">R.ID</th>
            <th scope="col" id="center_redsyskey">R.KEY</th>
            <th scope="col" style="width: 60px" id="center_redsys_currency">R.Divisa</th>
            <th scope="col" id="center_redsys_terminal">R.Terminal</th>
            <th scope="col" id="center_redsys_env">R.Entorno</th>
            <th scope="col" id="center_redsys_env">Especialidades</th>
            <th scope="col" id="center_redsys_env">Servicios</th>
        </tr>
        </thead>

        <tbody id="the-list">
        <?php foreach ($data as $datum) { ?>
            <tr id="post-13">
                <td class="sku column-sku" data-colname="SKU"><?php echo $datum['tsne'] ?>
                    <div>
                        <a href="admin.php?page=vithas-add-centros&id=<?php echo $datum['id'] ?>">Edit</a> |
                        <a class="text-danger"
                           href="admin.php?page=vithas-list-centros&delete=true&id=<?php echo $datum['id'] ?>">Eliminar</a>
                    </div>
                </td>
                <td class="sku column-sku" data-colname="SKU"><?php echo $datum['id_attr'] ?></td>
                <td class="sku column-sku" data-colname="SKU"><?php echo $datum['phone'] ?></td>
                <td class="sku column-sku" data-colname="SKU"><?php echo $datum['district'] ?></td>
                <td class="sku column-sku" data-colname="SKU"><?php echo $datum['mail'] ?></td>
                <td class="sku column-sku" data-colname="SKU"><?php echo $datum['mail_cc'] ?></td>
                <td class="sku column-sku" data-colname="SKU"><?php echo $datum['redsys_id'] ?></td>
                <td class="sku column-sku" data-colname="SKU"><?php echo $datum['redsys_key'] ?></td>
                <td class="sku column-sku" data-colname="SKU"><?php echo $datum['redsys_currency'] ?></td>
                <td class="sku column-sku" data-colname="SKU"><?php echo $datum['redsys_terminal'] ?></td>
                <td class="sku column-sku" data-colname="SKU"><?php echo $datum['redsys_env'] ?></td>
                <td class="sku column-sku" data-colname="SKU"><?php echo $datum['specialties'] ?></td>
                <td class="sku column-sku" data-colname="SKU"><?php echo $datum['type_service'] ?></td>


            </tr>
        <?php } ?>
        </tbody>

    </table>
</div>