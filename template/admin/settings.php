<?php
$data = (isset($setting)) ? $setting : [];
?>
<?php if ($_GET['log'] === 'view') { ?>
    <div class="code">
        <?php echo preg_replace("/\r\n|\r|\n/", '<br/>',
            file_get_contents(__DIR__ . '/../../LOG-ERROR.txt')); ?>
    </div>
<?php } elseif ($_GET['auto_attr'] === 'yes') { ?>
    <?php self::CheckTerm(); ?>
    <div class="code">
        Atributos iniciados
    </div>
<?php } else { ?>
    <div class="wrap">
        <div id="post-body-content" class="edit-form-section edit-comment-section">
            <div id="setting_submit" class="stuffbox">
                <div class="inside">
                    <h2 class="edit-comment-author">Ajustes</h2>
                    <fieldset>
                        <table class="form-table editcomment">
                            <tbody>
                            <tr>
                                <td class="first"><label for="version">Version</label></td>
                                <td><input type="text" disabled name="newcomment_author" size="30"
                                           value="<?php echo $data['version']->value ?>" id="version"></td>
                            </tr>
                            <tr>
                                <td class="first"><label for="redsys_ws">Redsys Hook</label></td>
                                <td><input type="text" name="redsys_ws" size="30"
                                           value="<?php echo $data['redsys_ws']->value ?>"
                                           id="redsys_ws"></td>
                            </tr>
                            <tr>
                                <td class="first"><label for="email">Auto-Grid</label></td>
                                <td>
                                    <input type="text" name="auto_grid" size="30"
                                           value="<?php echo $data['auto_grid']->value ?>" id="auto_grid">
                                </td>
                            </tr>
                            <tr>
                                <td class="first"><label for="newcomment_author_url">jQuery</label></td>
                                <td>
                                    <input type="text" id="jquery" name="jquery" size="30"
                                           value="<?php echo $data['jquery']->value ?>">
                                </td>
                            </tr>
                            <tr>
                                <td class="first"><label for="newcomment_author_url">Iniciar</label></td>
                                <td>
                                    <a href="/wp-admin/admin.php?page=vithas-settings&auto_attr=yes">Iniciar</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="first"><label for="newcomment_author_url">Log</label></td>
                                <td>
                                    <a target="_blank" href="/wp-admin/admin.php?page=vithas-settings&log=view">Ver</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </fieldset>
                </div>
            </div>

            <?php
            $content = $data['terms_conditions']->value;
            $editor_id = 'terms_conditions';

            wp_editor($content, $editor_id);
            ?>

            <button type="button" onClick="save_setting()" class="btn btn-default">Guardar</button>
        </div>
    </div>
<?php } ?>
