<?php
$js_name = (isset($js_name)) ? $js_name : 'error_' . time();
$fields = (isset($field_form)) ? $field_form : [];
$count = 0;
$len = count($fields);
?>
<div id="v2-vithas-modal" class="modal form-general-vithas">
    <?php include(__DIR__ . '/../layout/gdrp.php') ?>
    <?php include(__DIR__ . '/../layout/loading.php') ?>
    <?php include(__DIR__ . '/../layout/redsys_button.php') ?>

    <form id="submitSend">
        <?php include(__DIR__ . '/../layout/header.php') ?>

        <?php
        $html = '';

        function modalRawInput($field, $class = '')
        {
            $htm = '';
            if ($field['type'] === 'input') {
                $htm .= '<div class="vithas-flex ' . $class . '">';
                $htm .= '<div class="form-group mr-0">';
                $htm .= '<label for="' . $field['name'] . '">' . $field['label'] . ':</label>';
                $htm .= '<input type="text" required maxlength="40" class="form-control" id="' . $field['id'] . '">';
                $htm .= '</div>';
                $htm .= '</div>';
            } elseif ($field['type'] === 'select') {
                $htm .= '<div class="vithas-flex ' . $class . '">';
                $htm .= '<div class="form-group mr-0">';
                $htm .= '<label for="' . $field['name'] . '" class="_clase_'.$field['name'].'">' . $field['label'] . ':</label>';
                $htm .= '<select class="form-control" id="' . $field['name'] . '"></select>';
                $htm .= '</div>';
                $htm .= '</div>';
            };

            return $htm;
        }


        foreach ($fields as $field) {

            if (($count == $len - 1)) {
                $html .= modalRawInput($field, 'col-12');
            } else {
                $html .= modalRawInput($field, 'col-6');
            }

            $count++;
        }

        echo '<div class="col-12 m-0 row p-0">' . $html . '</div>';
        ?>
        <?php include(__DIR__ . '/../layout/buttons.php') ?>

        <?php include(__DIR__ . '/../layout/copies.php') ?>

    </form>
</div>
