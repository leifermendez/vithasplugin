<?php
$data = (isset($data)) ? $data : [];
$sku_info = (isset($sku_info)) ? $sku_info : [];
?>
<div class="grid-variations">
    <div id="header" class="d-flex justify-content-between">
        <span>Centro Vithas</span>
        <span></span>
    </div>

    <?php foreach ($data as $datum) { ?>
        <div class="d-flex justify-content-between grid-item">
            <div class="block">
                <h6 class="center-line"><?php echo $datum['name'] ?></h6>
            </div>
            <div class="block">
                <!-- SKU: <?php echo $datum['sku'] ?> -->
                <?php if($sku_info[$datum['ID']]['pay']){ ?>
                <div>
                    <button type="button" class="v2-vh-modal btn btn-success mb-1"
                            data-id-attr="<?php echo $datum['ID'] ?>"  data-id-center="<?php echo $sku_info[$datum['ID']]['center'] ?>">
                        Comprar ahora
                    </button>
                </div>
                <?php } ?>
                <div>
                    <button type="button" class="v2-vh-modal btn btn-primary"
                            data-id-attr="<?php echo $datum['ID'] ?>"
                            data-id-center="<?php echo $sku_info[$datum['ID']]['center'] ?>"
                            data-hide-pay="true">
                        Reservar ahora
                    </button>
                </div>
            </div>
        </div>
    <?php } ?>
</div>